# frozen_string_literal: true

class ProfileDecorator < SimpleDelegator
  def display_address
    "#{exists(street) ? "#{street}, " : nil}\
    #{exists(zip) ? zip : nil}\
    #{exists(city) ? "#{city}, " : nil}"
      .split.join(' ')
  end

  def exists(value)
    value.present?
  end
end
