# frozen_string_literal: true

class QuestionnaireDecorator < SimpleDelegator
  def display_address
    "#{exists(answer('street')) ? "#{answer('street')}, " : nil}\
    #{exists(answer('zip_code')) ? answer('zip_code') : nil}\
    #{exists(answer('city')) ? "#{answer('city')}, " : nil}"
      .split.join(' ')
  end

  def display_answer(answer_type)
    (exists(answer(answer_type)) ? answer(answer_type) : '-').to_s
                                                             .split.join(' ')
  end

  def exists(value)
    value.present?
  end
end
