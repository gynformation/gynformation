# frozen_string_literal: true

class PostDecorator < SimpleDelegator
  def display_title_credit
    if both_exist(main_image_title, main_image_credit)
      "#{main_image_title} / #{main_image_credit}"
    elsif title_exists(main_image_title, main_image_credit)
      main_image_title
    elsif credit_exists(main_image_title, main_image_credit)
      main_image_credit
    end
  end

  def both_exist(title, credit)
    exists(title) && exists(credit)
  end

  def title_exists(title, credit)
    exists(title) && not_exists(credit)
  end

  def credit_exists(title, credit)
    not_exists(title) && exists(credit)
  end

  def exists(value)
    value.present?
  end

  def not_exists(value)
    value.blank?
  end
end
