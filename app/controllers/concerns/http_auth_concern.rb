# frozen_string_literal: true

module HttpAuthConcern
  extend ActiveSupport::Concern

  included do
    before_action :http_authenticate
  end

  def http_authenticate
    return unless ENV['BASIC_AUTH'] == 'enabled'

    authenticate_or_request_with_http_basic do |username, password|
      username == Rails.application.credentials.basic_auth[:username] &&
        password == Rails.application.credentials.basic_auth[:password]
    end
  end
end
