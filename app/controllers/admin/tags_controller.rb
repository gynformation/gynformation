# frozen_string_literal: true

class Admin::TagsController < ApplicationController
  layout 'admin'

  def index
    @tags = Tag.all
  end

  def show
    @tag = Tag.find(params[:id])
  end

  def new
    @tag = Tag.new
  end

  def edit
    @tag = Tag.find(params[:id])
  end

  def create
    @tag = Tag.new(tag_params)
    @tag.key = parameterize_en_name
    if @tag.save
      redirect_to admin_tags_path
    else
      render 'new'
    end
  end

  def update
    @tag = Tag.find(params[:id])
    if @tag.update(tag_params)
      flash[:success] = 'Tag updated'
      redirect_to admin_tags_path
    else
      render 'edit'
    end
  end

  private

  def tag_params
    params.require(:tag)
          .permit(:name, :de, :en)
  end

  def parameterize_en_name
    params[:tag][:en].parameterize(separator: '_')
  end
end
