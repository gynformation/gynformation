# frozen_string_literal: true

class Admin::PostTagsController < ApplicationController
  layout 'admin'

  def index
    @post_tags = PostTag.all
  end

  def show
    @post_tag = PostTag.find(params[:id])
  end

  def new
    session[:return_to] ||= request.referer
    @post_tag = PostTag.new
  end

  def edit
    @post_tag = PostTag.find(params[:id])
  end

  def create
    @post_tag = PostTag.new(post_tag_params)

    @post_tag.key = parameterize_en_name
    if @post_tag.save
      if session[:return_to]
        redirect_to session.delete(:return_to)
      else
        redirect_to admin_post_tags_path
      end
    else
      render 'new'
    end
  end

  def update
    @post_tag = PostTag.find(params[:id])
    if @post_tag.update(post_tag_params)
      flash[:success] = 'PostTag updated'
      redirect_to admin_post_tags_path
    else
      render 'edit'
    end
  end

  private

  def post_tag_params
    params.require(:post_tag)
          .permit(:name, :de, :en)
  end

  def parameterize_en_name
    params[:post_tag][:en].parameterize(separator: '_')
  end
end
