# frozen_string_literal: true

class Admin::ProfilesController < ApplicationController
  layout 'admin'
  protect_from_forgery except: :search if Rails.env.test?

  def index
    @pagy, @profiles = pagy(
      Profile.filter(params.slice(:published, :last_name))
    )
  end

  def show
    @profile = Profile.find(params[:id])
  end

  def new
    @questionnaire = Questionnaire.find(params[:questionnaire_id]) if params[:questionnaire_id]

    if @questionnaire

      fill_profile(@questionnaire)

      if @profile.save
        @questionnaire.update(profile_id: @profile.id)
        redirect_to admin_profile_path(@profile)
      end

    else
      @profile = Profile.new
      @profile.build_address
      @profile.comments.build
    end
  end

  def edit
    @profile = Profile.find(params[:id])
  end

  def create
    @profile = Profile.new(profile_params)

    if @profile.save
      redirect_to admin_profile_path(@profile)
    else
      render 'new'
    end
  end

  def update
    @profile = Profile.find(params[:id])
    if @profile.update(profile_params)
      flash[:success] = 'Profile updated'
      redirect_to admin_profile_path(@profile)
    else
      render 'edit'
    end
  end

  def destroy
    @profile = Profile.find(params[:id])
    @profile.destroy
    redirect_to admin_profiles_path
  end

  def search
    @profiles = Profile.filter(params.slice(:last_name))
    respond_to do |format|
      format.js
    end
  end

  private

  def profile_params
    params.require(:profile).permit(
      :first_name, :last_name, :website_with_protocol, :category, :published,
      :language, :tag_list, :tag, { tag_ids: [] }, :tag_ids, :phone_number,
      :accessibility_addition, :financing_of_treatments, :internal_comment,
      :treatment_method_id, { treatment_method_ids: [] }, :treatment_method_ids,
      address_attributes: %i[id profile_id street zip city state],
      health_insurance: [], accessibility_options: [],
      comments_attributes: %i[content _destroy id]
    )
  end

  def fill_profile(_questionnaire)
    @profile = Profile.new(
      first_name: @questionnaire.answer('first_name'),
      last_name: @questionnaire.answer('last_name'),
      phone_number: @questionnaire.answer('phone_number'),
      website_with_protocol: @questionnaire.answer('website_with_protocol'),
      category: @questionnaire.answer('category'),
      health_insurance: @questionnaire.answer('health_insurance'),
      financing_of_treatments: @questionnaire.answer('payments_required_for_specific_treatments'),
      accessibility_addition: @questionnaire.answer('barrier_free_access'),
      accessibility_options: @questionnaire.answer('barrier_free_access_options'),
      language: @questionnaire.answer('languages'),
      tag_ids: @questionnaire.tags.map(&:id),
      treatment_method_ids: @questionnaire.treatment_methods.map(&:id)
    )

    @profile.build_address
    @profile.update(
      address_attributes: { street: @questionnaire.answer('street'), zip: @questionnaire.answer('zip_code'),
                            city: @questionnaire.answer('city'), state: @questionnaire.answer('state') }
    )

    if @questionnaire.answer('additional_recommended_treatments').present?
      @profile.comments.new(content: @questionnaire.answer('additional_recommended_treatments'))
    end
    if @questionnaire.answer('additional_self_describing_tags').present?
      @profile.comments.new(content: @questionnaire.answer('additional_self_describing_tags'))
    end
    if @questionnaire.answer('respect_anything_else').present?
      @profile.comments.new(content: @questionnaire.answer('respect_anything_else'))
    end

    @profile
  end
end
