# frozen_string_literal: true

class Admin::DashboardController < ApplicationController
  layout 'admin'
  def index
    @profiles = Profile.all
    @questionnaires = Questionnaire.all
  end
end
