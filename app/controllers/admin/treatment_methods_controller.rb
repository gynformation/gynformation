# frozen_string_literal: true

class Admin::TreatmentMethodsController < ApplicationController
  layout 'admin'

  def index
    @treatment_methods = TreatmentMethod.all
  end

  def show
    @treatment_method = TreatmentMethod.find(params[:id])
  end

  def new
    @treatment_method = TreatmentMethod.new
  end

  def edit
    @treatment_method = TreatmentMethod.find(params[:id])
  end

  def create
    @treatment_method = TreatmentMethod.new(treatment_method_params)
    @treatment_method.key = parameterize_en_name
    if @treatment_method.save
      redirect_to admin_treatment_methods_path
    else
      render 'new'
    end
  end

  def update
    @treatment_method = TreatmentMethod.find(params[:id])
    if @treatment_method.update(treatment_method_params)
      flash[:success] = 'TreatmentMethod updated'
      redirect_to admin_treatment_methods_path
    else
      render 'edit'
    end
  end

  private

  def treatment_method_params
    params.require(:treatment_method)
          .permit(:name, :de, :en)
  end

  def parameterize_en_name
    params[:treatment_method][:en].parameterize(separator: '_')
  end
end
