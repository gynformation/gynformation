# frozen_string_literal: true

class Admin::PostsController < ApplicationController
  layout 'admin'
  before_action :set_post, only: %i[show edit update destroy]

  def index
    @posts = Post.all
  end

  def show; end

  def new
    @post = Post.new
  end

  def edit; end

  def create
    @post = Post.new(post_params)

    if @post.save
      flash[:success] = 'Post was successfully created'
      redirect_to admin_post_path(@post)
    else
      render 'new'
    end
  end

  def update
    if @post.update(post_params)
      flash[:success] = 'Post was successfully updated'
      redirect_to admin_post_path(@post)
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    flash[:success] = 'Post was successfully destroyed'
    redirect_to admin_posts_path
  end

  private

  def set_post
    @post = Post.with_attached_other_images.find(params[:id])
  end

  def post_params
    params.require(:post).permit(
      :title, :main_image, :main_image_title, :main_image_credit,
      :publish_date, :body, :author, :teaser, :published,
      other_images: [], post_tag_ids: []
    )
  end
end
