# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Clearance::Controller
  include HttpAuthConcern
  include Pagy::Backend

  around_action :switch_locale

  def switch_locale(&)
    locale = params[:locale] || I18n.default_locale
    I18n.with_locale(locale, &)
  end

  private

  def default_url_options
    { locale: I18n.locale }
  end
end
