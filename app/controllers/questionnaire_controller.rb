# frozen_string_literal: true

class QuestionnaireController < ApplicationController
  invisible_captcha only: %i[create], honeypot: :subtitle

  def new
    @questionnaire_form = QuestionnaireForm.new
  end

  def create
    @questionnaire_form = QuestionnaireForm.new(questionnaire_form_params)
    if @questionnaire_form.save
      redirect_to questionnaire_info_path
    else
      render :new
    end
  end

  def info; end

  def questionnaire_form_params
    params.require(:questionnaire_form).permit(
      :first_name, :last_name, :category, :street, :zip_code, :city,
      :state, :website_with_protocol, :languages, :payments_required_for_specific_treatments,
      :treatment_rejected, :phone_number,
      :respect_anything_else, :feedback, :barrier_free_access, :amount_of_visits,
      :practice_staff, :limit_recommendation,
      :additional_recommended_treatments, :additional_self_describing_tags,
      examination_execution: [], treated_with_respect: %i[boolean comment],
      discriminatory_remarks: %i[boolean comment],
      explanation_of_treatment: %i[boolean comment],
      listening: %i[boolean comment], consent: %i[boolean comment],
      financing_of_treatments: %i[boolean comment],
      self_describing_tags: [], recommended_treatments: [], health_insurance: [],
      barrier_free_access_options: [],
      tag_ids: [], treatment_method_ids: []
    )
  end
end
