# frozen_string_literal: true

class PostsController < ApplicationController
  def index
    @pagy, @posts = pagy(Post.filter(
      params.slice(:post_tag)
    ).where(published: true))
  end

  def show
    @post = Post.where(published: true).find(params[:id])
    ahoy.track 'Viewed blog post', postId: params[:id]
  end
end
