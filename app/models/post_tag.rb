# frozen_string_literal: true

class PostTag < ApplicationRecord
  has_many :post_taggings, dependent: :destroy
  has_many :posts, through: :post_taggings

  store_accessor :name, :de, :en

  validates :en, presence: true
  validates :de, presence: true
end
