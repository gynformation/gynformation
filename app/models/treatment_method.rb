# frozen_string_literal: true

class TreatmentMethod < ApplicationRecord
  has_many :profile_treatments, dependent: :destroy
  has_many :profiles, through: :profile_treatments

  has_many :questionnaire_treatments, dependent: :destroy
  has_many :questionnaires, through: :questionnaire_treatments

  store_accessor :name, :de, :en

  validates :en, presence: true
  validates :de, presence: true
end
