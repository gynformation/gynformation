# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :taggings, dependent: :destroy
  has_many :profiles, through: :taggings

  has_many :questionnaire_taggings, dependent: :destroy
  has_many :questionnaires, through: :questionnaire_taggings

  store_accessor :name, :de, :en

  validates :en, presence: true
  validates :de, presence: true
end
