# frozen_string_literal: true

module Filterable
  extend ActiveSupport::Concern

  module ClassMethods
    def filter(filtering_params)
      results = where(nil)
      filtering_params.each do |key, value|
        value.compact_blank! if value.is_a? Array
        results = results.public_send("filter_by_#{key}", value) if value.present?
      end
      results
    end
  end
end
