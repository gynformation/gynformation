# frozen_string_literal: true

class QuestionnaireTagging < ApplicationRecord
  belongs_to :tag
  belongs_to :questionnaire
end
