# frozen_string_literal: true

class Profile < ApplicationRecord
  include Filterable
  has_one :address, dependent: :destroy
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings
  has_many :profile_treatments, dependent: :destroy
  has_many :treatment_methods, through: :profile_treatments
  has_many :questionnaires, dependent: :nullify
  has_many :comments, dependent: :destroy
  attribute :published, :boolean, default: false

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :comments, allow_destroy: true,
                                           reject_if: :all_blank

  delegate :street, :city, :state, :zip, to: :address, allow_nil: true

  enum category: { gynecologist: 0, general_practitioner: 1, midwife: 2,
                   endocrinologist: 3, urologist: 4, dermatologist: 5 }

  validates :last_name, presence: true
  validates :category, presence: true
  validates :website_with_protocol,
            format: {
              with: %r{\A\z|\Ahttps?://.*\z},
              message: I18n.t('activerecord.errors.models.profile.attributes.website_with_protocol.wrong_format')
            }, allow_nil: true

  scope :filter_by_tag, lambda { |filter_tags|
    joins(:tags).where('tags.key' => filter_tags).group(:id).having('COUNT(*) >= ?', filter_tags.length)
  }
  scope :filter_by_state, lambda { |state|
                            joins(:address)
                              .where('addresses.state' => state)
                          }
  scope :filter_by_treatment_method, lambda { |filter_treatment_methods|
    joins(:treatment_methods).where('treatment_methods.key': filter_treatment_methods)
                             .group(:id).having('COUNT(*) >= ?', filter_treatment_methods.length)
  }
  scope :filter_by_published, ->(published) { where published: }
  scope :filter_by_category, ->(category) { where category: }
  scope :filter_by_last_name,
        lambda { |last_name|
          where('lower(last_name) like ?', "%#{last_name.downcase}%")
        }

  def tag_list
    tags.map(&:name).join(', ')
  end
end
