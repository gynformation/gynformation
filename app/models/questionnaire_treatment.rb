# frozen_string_literal: true

class QuestionnaireTreatment < ApplicationRecord
  belongs_to :treatment_method
  belongs_to :questionnaire
end
