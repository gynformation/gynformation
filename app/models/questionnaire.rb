# frozen_string_literal: true

class Questionnaire < ApplicationRecord
  include Filterable

  attribute :status, :integer, default: 1
  has_many :answers, dependent: :destroy
  has_many :questionnaire_taggings, dependent: :destroy
  has_many :tags, through: :questionnaire_taggings
  has_many :questionnaire_treatments, dependent: :destroy
  has_many :treatment_methods, through: :questionnaire_treatments
  belongs_to :profile, optional: true
  enum status: { new: 1, check_1_completed: 2, consultation_required: 4,
                 profile_published: 5, in_progress: 6,
                 inquired_about_abortion: 7, rejected: 8 }, _prefix: :status

  default_scope { order(created_at: :desc) }
  scope :filter_by_status, ->(status) { where status: }
  scope :filter_by_last_name, lambda { |last_name|
                                includes(:answers)
                                  .where('answers.key' => 'last_name')
                                  .where('lower(answers.content::text) like ?',
                                         "%#{last_name.downcase}%")
                              }

  def treating_persons_name
    return unless names_present?

    answers.find_by(key: 'first_name').content << ' ' <<
      answers.find_by(key: 'last_name').content
  end

  def answer(key)
    answer = answers.find_by(key:) || NullAnswer.new(key)
    answer.content
  end

  private

  def names_present?
    answers.find_by(key: 'first_name') &&
      answers.find_by(key: 'last_name')
  end
end
