# frozen_string_literal: true

class QuestionnaireForm
  include ActiveModel::Model
  ATTRIBUTES = %i[first_name last_name category street zip_code
                  city state website_with_protocol payments_required_for_specific_treatments
                  languages barrier_free_access_options
                  barrier_free_access amount_of_visits health_insurance
                  self_describing_tags recommended_treatments treatment_rejected
                  financing_of_treatments listening consent
                  examination_execution treated_with_respect
                  discriminatory_remarks respect_anything_else feedback
                  additional_self_describing_tags explanation_of_treatment
                  additional_recommended_treatments practice_staff
                  limit_recommendation phone_number treatment_method_ids tag_ids].freeze

  attr_accessor *ATTRIBUTES

  def save
    return false if invalid?

    create_questionnaire
    true
  end

  private

  def create_questionnaire
    ActiveRecord::Base.transaction do
      questionnaire = Questionnaire.new
      ATTRIBUTES.each do |attribute|
        next unless attribute.to_s != 'tag_ids' && attribute.to_s != 'treatment_method_ids'

        questionnaire.answers.build(
          key: attribute.to_s, content: method(attribute).call
        )
      end
      questionnaire.save!

      create_taggings(questionnaire)
      create_treatments(questionnaire)
    end
  end

  def create_taggings(questionnaire)
    tag_ids = tags

    return if tag_ids.blank?

    tag_ids.compact_blank.each do |id|
      questionnaire.questionnaire_taggings.create!(tag_id: id)
    end
  end

  def create_treatments(questionnaire)
    treatment_method_ids = treatment_methods

    return if treatment_method_ids.blank?

    treatment_method_ids.compact_blank.each do |id|
      questionnaire.questionnaire_treatments.create!(treatment_method_id: id)
    end
  end

  def tags
    method('tag_ids').call
  end

  def treatment_methods
    method('treatment_method_ids').call
  end
end
