# frozen_string_literal: true

class Post < ApplicationRecord
  include Filterable
  validates :publish_date, presence: true
  has_rich_text :body
  has_one_attached :main_image
  has_many_attached :other_images
  has_many :post_taggings, dependent: :destroy
  has_many :post_tags, through: :post_taggings

  default_scope { order(publish_date: :desc) }

  scope :filter_by_post_tag, lambda { |key|
    joins(:post_tags).where('post_tags.key' => key)
  }
end
