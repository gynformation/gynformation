# frozen_string_literal: true

class Answer < ApplicationRecord
  default_scope { order(created_at: :asc) }
end
