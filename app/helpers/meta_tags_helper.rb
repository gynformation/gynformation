# frozen_string_literal: true

module MetaTagsHelper
  def meta_title
    if content_for?(:meta_title)
      content_for(:meta_title)
    else
      t(DEFAULT_META['meta_title'])
    end
  end

  def meta_description
    if content_for?(:meta_description)
      content_for(:meta_description)
    else
      t(DEFAULT_META['meta_description'])
    end
  end

  def meta_image
    if content_for?(:meta_image)
      content_for(:meta_image)
    else
      DEFAULT_META['meta_image']
    end
  end
end
