# frozen_string_literal: true

module ApplicationHelper
  include Pagy::Frontend

  def link_to_add_fields(name, form, association, css_class)
    new_object = form.object.send(association).klass.new
    id = new_object.object_id
    fields = form.fields_for(association, new_object, child_index: id) do |builder|
      render("#{association.to_s.singularize}_fields", form: builder)
    end
    link_to(name, '#', class: "add_fields #{css_class}",
                       data: { id:, fields: fields.gsub("\n", '') })
  end

  def title
    if content_for?(:title)
      content_for :title
    else
      t("#{controller_path.tr('/', '.')}.#{action_name}.title",
        default: :site_name)
    end
  end
end
