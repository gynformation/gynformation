# frozen_string_literal: true

module ProfilesHelper
  def options_for_categories
    {
      t('profile.categories.gynecologist') => :gynecologist,
      t('profile.categories.general_practitioner') => :general_practitioner,
      t('profile.categories.midwife') => :midwife,
      t('profile.categories.endocrinologist') => :endocrinologist,
      t('profile.categories.urologist') => :urologist,
      t('profile.categories.dermatologist') => :dermatologist
    }
  end

  def state_name(iso_code)
    state_hash = Address.states.select { |state| state[:iso_code] == iso_code }
    state_hash.first[:name] unless state_hash.empty?
  end

  def check_if_selected(params, value)
    params.include?(value) if !params.nil? && params.length.positive?
  end
end
