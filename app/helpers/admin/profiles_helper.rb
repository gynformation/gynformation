# frozen_string_literal: true

module Admin::ProfilesHelper
  def profile_display_name(profile_id)
    profile = Profile.find(profile_id)
    "#{profile.first_name} #{profile.last_name}, #{profile.address.city}"
  end
end
