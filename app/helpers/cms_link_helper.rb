# frozen_string_literal: true

module CmsLinkHelper
  def cms_link(slug)
    comfy_cms_render_page_path(
      Comfy::Cms::Site.first.pages.find_by(slug:).slug
    )
  end

  def translated_cms_page_label(page)
    if page.translations.pluck(:locale).include?(I18n.locale.to_s)
      translated_page = page.translate!
      translated_page.label
    else
      page.label
    end
  end
end
