# frozen_string_literal: true

module QuestionnaireHelper
  def insurance_options
    {
      statutory: t('questionnaire_form.statutory'),
      private: t('questionnaire_form.private'),
      unknown: t('questionnaire_form.unknown')
    }
  end

  def tag_list
    {
      fgmc: t('questionnaire_form.tags.fgmc'),
      non_binary: t('questionnaire_form.tags.non_binary'),
      trans: t('questionnaire_form.tags.trans'),
      trans_masculinity: t('questionnaire_form.tags.trans_masculinity'),
      trans_femininity: t('questionnaire_form.tags.trans_femininity'),
      masculine_read: t('questionnaire_form.tags.masculine_read'),
      inter: t('questionnaire_form.tags.inter'),
      black_people: t('questionnaire_form.tags.black_people'),
      poc: t('questionnaire_form.tags.poc'),
      indigenous: t('questionnaire_form.tags.indigenous'),
      jewish: t('questionnaire_form.tags.jewish'),
      mentally_retarded: t('questionnaire_form.tags.mentally_retarded'),
      migrant: t('questionnaire_form.tags.migrant'),
      muslim: t('questionnaire_form.tags.muslim'),
      neurodivers: t('questionnaire_form.tags.neurodivers'),
      physically_disabled: t('questionnaire_form.tags.physically_disabled'),
      roma: t('questionnaire_form.tags.roma'),
      gay_bi_or_pan: t('questionnaire_form.tags.gay_bi_or_pan'),
      asexual: t('questionnaire_form.tags.asexual'),
      polyamorous: t('questionnaire_form.tags.polyamorous'),
      sex_worker: t('questionnaire_form.tags.sex_worker'),
      single_parent: t('questionnaire_form.tags.single_parent'),
      childfree_by_choice: t('questionnaire_form.tags.childfree_by_choice'),
      drug_user: t('questionnaire_form.tags.drug_user'),
      fat: t('questionnaire_form.tags.fat'),
      thin: t('questionnaire_form.tags.thin'),
      hiv_positive: t('questionnaire_form.tags.hiv_positive'),
      chronically_ill: t('questionnaire_form.tags.chronically_ill'),
      mental_illness: t('questionnaire_form.tags.mental_illness'),
      sexual_violence: t('questionnaire_form.tags.sexual_violence')
    }
  end

  def treatments_list
    {
      abortion_dileation: t('questionnaire_form.treatments.abortion_dileation'),
      abortion_aspiration: t('questionnaire_form.treatments.abortion_aspiration'),
      abortion_medication: t('questionnaire_form.treatments.abortion_medication'),
      contraception_methods: t('questionnaire_form.treatments.contraception_methods'),
      copper_chain_insertion: t('questionnaire_form.treatments.copper_chain_insertion'),
      spiral_insertion: t('questionnaire_form.treatments.spiral_insertion'),
      sterilization: t('questionnaire_form.treatments.sterilization'),
      transition: t('questionnaire_form.treatments.transition'),
      transition_hormone_therapy: t('questionnaire_form.treatments.transition_hormone_therapy'),
      neo_vagina: t('questionnaire_form.treatments.neo_vagina'),
      metoidioplasty: t('questionnaire_form.treatments.metoidioplasty'),
      people_with_penis: t('questionnaire_form.treatments.people_with_penis'),
      mastectomy: t('questionnaire_form.treatments.mastectomy'),
      breast_augmentation: t('questionnaire_form.treatments.breast_augmentation'),
      pregnancy: t('questionnaire_form.treatments.pregnancy'),
      miscarriage: t('questionnaire_form.treatments.miscarriage'),
      midwives_cooperation:
      t('questionnaire_form.treatments.midwives_cooperation'),
      out_of_hospital_births:
      t('questionnaire_form.treatments.out_of_hospital_births'),
      breastfeeding: t('questionnaire_form.treatments.breastfeeding'),
      chestfeeding: t('questionnaire_form.treatments.chestfeeding'),
      birth_preparation_classes_non_heteronormative:
      t('questionnaire_form.treatments.birth_preparation_classes_non_heteronormative'),
      birth_preparation_classes_single_parent:
      t('questionnaire_form.treatments.birth_preparation_classes_single_parent'),
      unfulfilled_desire_children_hetero:
      t('questionnaire_form.treatments.unfulfilled_desire_children_hetero'),
      unfulfilled_desire_children_queer:
      t('questionnaire_form.treatments.unfulfilled_desire_children_queer'),
      unfulfilled_desire_children_singles:
      t('questionnaire_form.treatments.unfulfilled_desire_children_singles'),
      alternative_treatments:
      t('questionnaire_form.treatments.alternative_treatments'),
      breast_disease: t('questionnaire_form.treatments.breast_disease'),
      cancer: t('questionnaire_form.treatments.cancer'),
      dealing_with_sexualized_violence:
      t('questionnaire_form.treatments.dealing_with_sexualized_violence'),
      endometriosis: t('questionnaire_form.treatments.endometriosis'),
      incontinence: t('questionnaire_form.treatments.incontinence'),
      routine_treatment: t('questionnaire_form.treatments.routine_treatment'),
      initial_treatment: t('questionnaire_form.treatments.initial_treatment'),
      menopause: t('questionnaire_form.treatments.menopause'),
      menstrual_symptoms: t('questionnaire_form.treatments.menstrual_symptoms'),
      mental_counseling: t('questionnaire_form.treatments.mental_counseling'),
      sexual_dysfunction: t('questionnaire_form.treatments.sexual_dysfunction'),
      std: t('questionnaire_form.treatments.std'),
      std_test: t('questionnaire_form.treatments.std_test'),
      vaginism: t('questionnaire_form.treatments.vaginism'),
      hormonal_expertise: t('questionnaire_form.treatments.hormonal_expertise'),
      dysplasia_consultation: t('questionnaire_form.treatments.dysplasia_consultation'),
      conization: t('questionnaire_form.treatments.conization'),
      cryopreservation: t('questionnaire_form.treatments.cryopreservation')
    }
  end

  def barrier_free_access_options_list
    {
      wheelchair_access: t('questionnaire_form.barrier_free_access_options.wheelchair_access'),
      orientation_aids: t('questionnaire_form.barrier_free_access_options.orientation_aids'),
      sign_language: t('questionnaire_form.barrier_free_access_options.sign_language'),
      easy_language_html: t('questionnaire_form.barrier_free_access_options.easy_language_html'),
      appointments_online: t('questionnaire_form.barrier_free_access_options.appointments_online'),
      appointments_phone: t('questionnaire_form.barrier_free_access_options.appointments_phone'),
      website_usable_with_screenreader:
      t('questionnaire_form.barrier_free_access_options.website_usable_with_screenreader')
    }
  end

  def boolean_options
    {
      yes: t('questionnaire_form.answer_yes'),
      no: t('questionnaire_form.answer_no')
    }
  end

  def financing_of_treatment_options
    {
      yes: t('questionnaire_form.answer_yes'),
      no: t('questionnaire_form.answer_no'),
      not_applicable: t('questionnaire_form.answer_not_applicable')
    }
  end

  def examination_execution_options
    {
      examine_yourself:
      t('questionnaire_form.examination_execution.examine_yourself'),
      lateral_position:
      t('questionnaire_form.examination_execution.lateral_position'),
      stay_clothed: t('questionnaire_form.examination_execution.stay_clothed')
    }
  end

  def category_options
    {
      t('questionnaire_form.category.gynecologist') => :gynecologist,
      t('questionnaire_form.category.general_practitioner') =>
      :general_practitioner,
      t('questionnaire_form.category.midwife') => :midwife,
      t('questionnaire_form.category.endocrinologist') => :endocrinologist,
      t('questionnaire_form.category.urologist') => :urologist,
      t('questionnaire_form.category.dermatologist') => :dermatologist
    }
  end
end
