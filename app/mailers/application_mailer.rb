# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'kontakt@gynformation.de'
  layout 'mailer'
end
