# frozen_string_literal: true
require_relative 'mapping'

namespace :questionnaire do
  desc 'Change the answer structure for existing answers to hash with boolean
    and comment key'
  task add_comment_to_existing_boolean_answers: :environment do
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to change the answer structure for existing answers to hash with boolean
    and comment key')

    start_time = Time.now 
    affected_answers = Answer.all.where(key: ['listening', 'consent', 'financing_of_treatments'])

    affected_answers.each do |answer| 
      unless answer.content.is_a?(Hash)
        old_content = answer.content
        answer.update!(content: { boolean: old_content, comment: ''})
      end
    end

    log("Task completed in #{Time.now - start_time} seconds.")
  end

  desc 'Copy answers into content json'
  task copy_answers_into_content_json: :environment do 
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to copy answers into content json')

    affected_questionnaires = Questionnaire.where(content: nil)

    affected_questionnaires.each do |questionnaire|
      QuestionnaireForm::ATTRIBUTES.each_with_object({}) do |attribute, hash|
        hash[attribute] = questionnaire.answers.where(key: attribute).first.content.presence || ""
        questionnaire .update!(content: hash)
      end
    end
  end

  desc 'Migrate tags from answers to taggings'
  task migrate_tags_from_answers_to_taggings: :environment do 
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to migrate tags from answers to taggings')

    affected_answers = Answer.all.where(key: ['self_describing_tags'])
        
    affected_answers.each do |answer|
      unless answer.content.blank?
        answer.content.each do |tag|
          matching_tag = Tag.all.where(key: tags[tag.to_sym]).first
          unless matching_tag.blank?
            QuestionnaireTagging.create!(tag_id: matching_tag.id, questionnaire_id: answer.questionnaire_id)
          end
        end
      end
    end
  end

  desc 'Migrate treatment methods from answers to treatments'
  task migrate_treatment_methods_from_answers_to_treatments: :environment do 
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to migrate treatment methods from answers to treatments')

    affected_answers = Answer.all.where(key: ['recommended_treatments'])

    affected_answers.each do |answer|
      unless answer.content.blank?
        answer.content.each do |treatment_method|
          matching_treatment_method = TreatmentMethod.all.where(key: treatments_list[treatment_method.to_sym]).first
          unless matching_treatment_method.blank?
            QuestionnaireTreatment.create!(treatment_method_id: matching_treatment_method.id, questionnaire_id: answer.questionnaire_id)
          end
        end
      end
    end
  end
end
