# frozen_string_literal: true

namespace :profiles do
  desc 'migrate comment field entries into comment model'
  task migrate_comments: :environment do
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to migrate comment field entries into comment model')

    start_time = Time.now 
    profiles = Profile.all

    profiles.each do |profile| 
      profile.comments.create!(content: profile.comment)
    end

    log("Task completed in #{Time.now - start_time} seconds.")
  end
end
