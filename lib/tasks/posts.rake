# frozen_string_literal: true

namespace :posts do
  desc 'Use created_at as publish_date'
  task use_created_at_as_publish_date: :environment do
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to use the created at date as publish date')

    start_time = Time.now 
    affected_posts = Post.all.where(publish_date: nil)

    affected_posts.each do |post| 
      post.update(publish_date: post.created_at)
    end

    log("Task completed in #{Time.now - start_time} seconds.")
  end
end
