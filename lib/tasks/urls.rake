# frozen_string_literal: true
require 'uri'
require 'net/http'
require 'openssl'

namespace :profiles do
  desc 'migrate URLs to include protocol'
  task migrate_urls: :environment do
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to migrate URLs to include protocol')

    start_time = Time.now 
    profiles = Profile.all

    failed_attempts = []

    profiles.each do |profile| 
      if profile.website_with_protocol == nil
        return
      end

      if URI(profile.website_with_protocol).scheme == nil
        https_url = "https://#{profile.website_with_protocol}"

        # check if https works
        begin
          log("Pinging #{https_url}")
          Net::HTTP.get_response(URI(https_url))
          profile.update(website_with_protocol: https_url)
          log("#{https_url} works")
        rescue OpenSSL::SSL::SSLError => _
          # it didn't, we try http instead and persist if that works
          http_url = "http://#{profile.website_with_protocol}"

          log("Pinged #{https_url} and it was not successful, trying #{http_url}")
          begin
            Net::HTTP.get_response(URI(http_url))
            profile.update(website_with_protocol: http_url)
            log("#{http_url} works")
          rescue
            log("Failed to ping #{profile.website_with_protocol}")
            failed_attempts.push(profile.website_with_protocol)
          end
        rescue => _
          log("Failed to ping #{profile.website_with_protocol} because the domain doesn't exist")
          failed_attempts.push(profile.website_with_protocol)
        end

      end
    end

    log("Task completed in #{Time.now - start_time} seconds.")
    log("Encountered #{failed_attempts.length} broken URLs: #{failed_attempts.join()}")
  end
end
