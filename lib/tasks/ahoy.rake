# frozen_string_literal: true

namespace :ahoy do
  desc 'Remove all user data from more than 2 years ago from ahoy visits'
  task remove_user_data_from_ahoy_visits: :environment do
    Ahoy::Visit.where("started_at < ?", 2.years.ago).find_in_batches do |visits|
      visit_ids = visits.map(&:id)
      Ahoy::Event.where(visit_id: visit_ids).delete_all
      Ahoy::Visit.where(id: visit_ids).delete_all
    end
  end
end
