class AddKeyToTag < ActiveRecord::Migration[5.2]
  def change
    add_column :tags, :key, :string
  end
end
