class AddAccessibilityOptionsToProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :accessibility_options, :string, array: true, default: []
  end
end
