class RenameAccessibilityOnProfile < ActiveRecord::Migration[6.0]
  def change
    rename_column :profiles, :accessibility, :accessibility_addition
  end
end
