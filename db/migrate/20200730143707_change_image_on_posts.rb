class ChangeImageOnPosts < ActiveRecord::Migration[6.0]
  def change
    change_table :posts do |t|
      t.rename :image_title, :main_image_title
      t.rename :image_credit, :main_image_credit
    end
  end
end
