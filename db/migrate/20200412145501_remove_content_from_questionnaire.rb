class RemoveContentFromQuestionnaire < ActiveRecord::Migration[5.2]
  def change
    remove_column :questionnaires, :content, :jsonb
  end
end
