# frozen_string_literal: true

class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.string :key
      t.jsonb :content
      t.belongs_to :questionnaire, foreign_key: true

      t.timestamps
    end
    add_index :answers, :questionnaire_id unless index_exists?(:answers, :questionnaire_id)
  end
end
