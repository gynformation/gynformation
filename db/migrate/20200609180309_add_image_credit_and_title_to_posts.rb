class AddImageCreditAndTitleToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :image_credit, :string
    add_column :posts, :image_title, :string
  end
end
