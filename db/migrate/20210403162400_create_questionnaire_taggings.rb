class CreateQuestionnaireTaggings < ActiveRecord::Migration[5.2]
  def change
    create_table :questionnaire_taggings do |t|
      t.belongs_to :tag, foreign_key: true
      t.belongs_to :questionnaire, foreign_key: true

      t.timestamps
    end
  end
end
