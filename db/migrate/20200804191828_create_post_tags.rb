class CreatePostTags < ActiveRecord::Migration[6.0]
  def change
    create_table :post_tags do |t|
      t.jsonb :name
      t.string :key

      t.timestamps
    end
  end
end
