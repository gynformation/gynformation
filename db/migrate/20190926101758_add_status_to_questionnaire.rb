class AddStatusToQuestionnaire < ActiveRecord::Migration[5.2]
  def change
    add_column :questionnaires, :status, :integer
    add_column :questionnaires, :comment, :string
    add_index :questionnaires, :status
  end
end
