class AddProfileToQuestionnaire < ActiveRecord::Migration[5.2]
  def change
    add_reference :questionnaires, :profile, foreign_key: true
  end
end
