class CreateQuestionnaireTreatments < ActiveRecord::Migration[5.2]
  def change
    create_table :questionnaire_treatments do |t|
      t.belongs_to :treatment_method, foreign_key: true
      t.belongs_to :questionnaire, foreign_key: true

      t.timestamps
    end
  end
end
