class AddWebsiteWithProtocolToProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :website_with_protocol, :string
    Profile.update_all("website_with_protocol=website")
  end
end
