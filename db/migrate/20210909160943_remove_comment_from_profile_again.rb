class RemoveCommentFromProfileAgain < ActiveRecord::Migration[6.0]
  def change
    remove_column :profiles, :comment, :string
  end
end
