class AddContentToQuestionnaire < ActiveRecord::Migration[5.2]
  def change
    add_column :questionnaires, :content, :jsonb
  end
end
