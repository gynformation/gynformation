class ChangeTagNameColumnDefault < ActiveRecord::Migration[5.2]
  def up
    change_column_default :tags, :name, {}
  end

  def down
    change_column_default :tags, :name, '{}'
  end
end
