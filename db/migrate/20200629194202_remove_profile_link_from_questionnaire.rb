class RemoveProfileLinkFromQuestionnaire < ActiveRecord::Migration[5.2]
  def change
    remove_column :questionnaires, :profile_link, :string
  end
end
