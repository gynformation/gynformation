class AddInternalCommentFieldToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :internal_comment, :string
  end
end
