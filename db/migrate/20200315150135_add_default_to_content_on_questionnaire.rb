class AddDefaultToContentOnQuestionnaire < ActiveRecord::Migration[5.2]
  def change
    change_column_default :questionnaires, :content, from: nil, to: {}
  end
end
