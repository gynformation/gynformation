class AddPublishDateToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :publish_date, :date
  end
end
