class AddWebsiteToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :website, :string
  end
end
