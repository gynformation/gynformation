# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.2'
gem 'rails', '~> 7.0.3.1'

gem 'net-imap', require: false
gem 'net-pop', require: false
gem 'net-smtp', require: false

gem 'ahoy_matey', '~> 4.0'
gem 'aws-sdk-s3', require: false
gem 'blazer'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'bulma-rails', '~> 0.7.5'
gem 'clearance'
gem 'comfortable_mexican_sofa', git: 'https://github.com/restarone/comfortable-mexican-sofa.git'
gem 'comfy_blog', '~> 2.0.0'
gem 'faker', '~> 2.0'
gem 'honeybadger', '~> 4.0'
gem 'image_processing', '~> 1.2'
gem 'invisible_captcha'
gem 'jbuilder', '~> 2.5'
# tolk uses kaminari
gem 'kaminari'
gem 'letter_opener'
gem 'pagy'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5.0'
gem 'tolk'
gem 'turbolinks', '~> 5'
gem 'will_paginate', '~> 3.1.0'
gem 'will-paginate-i18n'

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'uglifier', '>= 1.3.0'
gem 'webpacker'

group :development, :test do
  gem 'bundler-audit'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails'
end

group :development do
  gem 'brakeman', require: false
  gem 'listen'
  gem 'pry'
  gem 'rubocop'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'launchy'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'webdrivers'
end
