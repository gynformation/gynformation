# frozen_string_literal: true

require('rails_helper')

describe ProfilesHelper do
  describe 'profile_display_name' do
    it 'returns the full name and city' do
      address = create(:address, city: 'Hamburg')
      profile = create(:profile,
                       first_name: 'Lora',
                       last_name: 'Ipsum',
                       address:)
      expect(
        helper.profile_display_name(profile.id)
      ).to eq 'Lora Ipsum, Hamburg'
    end
  end
end
