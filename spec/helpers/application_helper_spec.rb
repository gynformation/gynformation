# frozen_string_literal: true

require('rails_helper')

RSpec.describe ApplicationHelper, type: :helper do
  describe '#title' do
    it 'should show fallback when no title is present' do
      allow(controller).to receive(:controller_path).and_return('questionnaire')
      allow(controller).to receive(:action_name).and_return('info')
      expect(helper.title).to eq('Gynformation')
    end

    it 'should show correct title for profiles list' do
      allow(controller).to receive(:controller_path).and_return('profiles')
      allow(controller).to receive(:action_name).and_return('index')
      expect(helper.title).to eq('Search')
    end

    it 'should show correct title for recommend page' do
      allow(controller).to receive(:controller_path).and_return('questionnaire')
      allow(controller).to receive(:action_name).and_return('new')
      expect(helper.title).to eq('Recommend')
    end
  end

  describe '#link_to_add_fields' do
    it 'returns a link with data-id and data-fields attributes' do
      profile = Profile.new
      template = double('Template')
      allow(template).to receive(:fields_for)
      form = ActionView::Helpers::FormBuilder.new(:profile, profile, template, {})

      link = helper.link_to_add_fields(
        'Add field', form, :comments, 'a-css-class'
      )

      expect(link).to include('data-id')
      expect(link).to include('data-fields')
      expect(link).to include('<a class="add_fields a-css-class"')
    end
  end
end
