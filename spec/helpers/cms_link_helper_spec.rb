# frozen_string_literal: true

require('rails_helper')

describe ProfilesHelper do
  describe '#translated_cms_page_label' do
    it 'returns the localized page label' do
      site = create(:cms_site, locale: 'de')
      layout = create(:cms_layout, site:)
      page = create(:cms_page, layout:, site:)
      create(
        :cms_translation,
        page:,
        locale: 'en',
        label: 'Translated label'
      )

      I18n.with_locale(:en) do
        expect(helper.translated_cms_page_label(page)).to eq 'Translated label'
      end
    end

    it 'returns the page label in the default locale when no translation exists' do
      site = create(:cms_site, locale: 'de')
      layout = create(:cms_layout, site:)
      page = create(:cms_page, layout:, site:, label: 'Label in default locale')

      I18n.with_locale(:en) do
        expect(helper.translated_cms_page_label(page)).to eq 'Label in default locale'
      end
    end
  end
end
