# frozen_string_literal: true

require('rails_helper')

RSpec.describe MetaTagsHelper, type: :helper do
  before do
    allow(helper).to receive(:t).with('.title')
                                .and_return('Default Title')
    allow(helper).to receive(:t).with('meta.description')
                                .and_return('Default Description')
  end

  it 'returns valid meta title and description when present' do
    allow(helper).to receive(:meta_title)
      .and_return('A new and specific title for the page')
    allow(helper).to receive(:meta_description)
      .and_return('A new and exciting description for the page')
    allow(helper).to receive(:meta_image)
      .and_return(asset_path('condoms.png'))

    expect(helper.meta_title)
      .to eq 'A new and specific title for the page'
    expect(helper.meta_description)
      .to eq 'A new and exciting description for the page'
    expect(helper.meta_image)
      .to eq asset_path('condoms.png')
  end

  it 'returns fallback meta image when none is present' do
    expect(helper.meta_title)
      .to eq 'Default Title'
    expect(helper.meta_description)
      .to eq 'Default Description'
    expect(helper.meta_image)
      .to eq 'asset_path(\'treatment_chair.png\')'
  end
end
