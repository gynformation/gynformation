# frozen_string_literal: true

FactoryBot.define do
  factory :tag do
    key { 'trans_friendly' }
    name { { en: 'Trans friendly', de: 'Trans freundlich' } }
  end
end
