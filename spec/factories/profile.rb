# frozen_string_literal: true

FactoryBot.define do
  factory :profile do
    first_name { 'Lora' }
    last_name { 'Ipsum' }
    website_with_protocol { 'https://www.example.org' }
    comments { [] }
    language { 'English' }
    phone_number { '2923483248234234' }
    category { 1 }
    taggings { [] }
    accessibility_options { ['wheelchair_access'] }
    accessibility_addition { 'Some additional information about accessibility' }
    financing_of_treatments { 'Information about financing of treatments' }
    health_insurance { %i[private] }

    factory :published_profile do
      first_name { 'Petra' }
      last_name { 'Published' }
      website_with_protocol { 'https://www.example.org' }
      language { 'English' }
      published { true }
    end

    factory :unpublished_profile do
      first_name { 'Ute' }
      last_name { 'Unpublished Lastname' }
      website_with_protocol { 'https://www.example.org' }
      language { 'Spanish, German' }
      published { false }
    end
  end
end
