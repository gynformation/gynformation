# frozen_string_literal: true

FactoryBot.define do
  factory :questionnaire_tagging do
    tag { nil }
    questionnaire { nil }
  end
end
