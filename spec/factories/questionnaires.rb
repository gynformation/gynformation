# frozen_string_literal: true

FactoryBot.define do
  factory :questionnaire do
    answers { [] }
    status  { 1 }
    comment { 'This is a comment' }

    factory :new_questionnaire do
      status  { 1 }
      comment { 'This is a new questionnaire' }
    end

    factory :check_1_completed_questionnaire do
      status  { 2 }
      comment { 'This is a check_1_completed questionnaire' }
    end

    factory :consultation_required_questionnaire do
      status  { 4 }
      comment { 'This is a consultation_required questionnaire' }
    end

    factory :profile_published_questionnaire do
      status  { 5 }
      comment { 'This is a profile_published questionnaire' }
    end

    factory :inquired_about_abortion_questionnaire do
      status  { 7 }
      comment { 'This is a inquired_about_abortion questionnaire' }
    end
  end
end
