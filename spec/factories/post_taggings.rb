# frozen_string_literal: true

FactoryBot.define do
  factory :post_tagging do
    post_tag { nil }
    post { nil }
  end
end
