# frozen_string_literal: true

FactoryBot.define do
  factory :cms_site, class: Comfy::Cms::Site do
    label { 'My Website' }
    identifier { 'my-website' }
    hostname { 'gynformation.de' }
    path { '' }
    locale { 'de' }
  end
end
