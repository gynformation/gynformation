# frozen_string_literal: true

FactoryBot.define do
  factory :cms_translation, class: Comfy::Cms::Translation do
    locale { 'en' }
    label { 'This is a translation of a label' }
  end
end
