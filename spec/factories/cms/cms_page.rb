# frozen_string_literal: true

FactoryBot.define do
  factory :cms_page, class: Comfy::Cms::Page do
    label { 'label' }
    slug { 'slug' }
  end
end
