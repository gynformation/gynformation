# frozen_string_literal: true

FactoryBot.define do
  factory :post, class: 'Post' do
    title { 'This is an awesome blogpost' }
    body do
      'This is the content of the blog post.'
    end
    author { 'Test Author' }
    teaser do
      'This is a teaser to let people now what the blog post is about'
    end
    publish_date { '2020-06-06' }
    main_image_credit { 'Test credit' }
    main_image_title do
      'This is the title of the image.'
    end
    factory :published_post do
      published { true }
    end
    post_taggings { [] }
  end

  factory :post_no_main_image_credit, class: 'Post' do
    title { 'This is an awesome blogpost' }
    body do
      'This is the content of the blog post.'
    end
    author { 'Test Author' }
    teaser do
      'This is a teaser to let people now what the blog post is about'
    end
    publish_date { '2020-06-06' }
    main_image_title do
      'This is the title of the image.'
    end
  end

  factory :post_no_main_image_title, class: 'Post' do
    title { 'This is an awesome blogpost' }
    body do
      'This is the content of the blog post.'
    end
    author { 'Test Author' }
    teaser do
      'This is a teaser to let people now what the blog post is about'
    end
    publish_date { '2020-06-06' }
    main_image_credit { 'Test credit' }
  end
end
