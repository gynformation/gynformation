# frozen_string_literal: true

FactoryBot.define do
  factory :address do
    street { 'Bakerstreet 121' }
    zip { '20148' }
    city { 'Hamburg' }
    state { 'DE-HH' }
    profile_id { 1 }

    factory :address_without_street do
      street { '' }
      zip { '20148' }
      city { 'Hamburg' }
      state { 'DE-HH' }
      profile_id { 1 }
    end

    factory :address_without_zip do
      street { '' }
      zip { '' }
      city { 'Hamburg' }
      state { 'DE-HH' }
      profile_id { 1 }
    end

    factory :address_without_city do
      street { '' }
      zip { '' }
      city { '' }
      state { 'DE-HH' }
      profile_id { 1 }
    end
  end
end
