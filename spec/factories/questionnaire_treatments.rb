# frozen_string_literal: true

FactoryBot.define do
  factory :questionnaire_treatment do
    treatment_method { nil }
    questionnaire { nil }
  end
end
