# frozen_string_literal: true

FactoryBot.define do
  factory :profile_treatment do
    treatment_method { nil }
    profile { nil }
  end
end
