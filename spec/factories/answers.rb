# frozen_string_literal: true

FactoryBot.define do
  factory :answer do
    key { 'MyString' }
    content { '' }
  end
end
