# frozen_string_literal: true

FactoryBot.define do
  factory :post_tag do
    key { 'information' }
    name { { en: 'Information', de: 'Information' } }
  end
end
