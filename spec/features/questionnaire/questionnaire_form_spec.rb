# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Submitting a questionnaire', type: :feature do
  it 'redirects to info page after successful submission' do
    @treatment_method = create(:treatment_method,
                               name: { en: 'Abortion', de: 'Schwangerschaftsabbruch' })
    @tag1 = create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    @tag2 = create(:tag, name: { en: 'Fat friendly', de: 'Fat freundlich' })

    visit new_questionnaire_path

    next_button = find(:css, '[data-test="next-step"]')
    next_button.click
    fill_in_step1
    next_button.click
    fill_in_step2
    next_button.click
    fill_in_step3
    next_button.click
    fill_in_step4
    next_button.click
    fill_in 'questionnaire_form[feedback]', with: 'my feedback'

    click_button I18n.t('questionnaire_form.submit')

    expect(page).to have_content(I18n.t('questionnaire.info.headline'))
  end

  it 'contains a honeypot field for bots' do
    visit new_questionnaire_path

    expect(page).to have_selector('input#questionnaire_form_subtitle',
                                  visible: false)
  end

  def fill_in_step1
    fill_in 'questionnaire_form[first_name]', with: 'Lora'
    fill_in 'questionnaire_form[last_name]', with: 'Ipsum'
    select 'Midwife', from: 'questionnaire_form_category'
    fill_in 'questionnaire_form[street]', with: 'Example Street'
    fill_in 'questionnaire_form[zip_code]', with: '12345'
    fill_in 'questionnaire_form[city]', with: 'Berlin'
    select 'Berlin', from: 'questionnaire_form_state'
    fill_in 'questionnaire_form[website_with_protocol]', with: 'https://www.example.org'
    fill_in 'questionnaire_form[phone_number]', with: '123450-3333'
    fill_in 'questionnaire_form[languages]', with: 'spanish, english'
    fill_in 'questionnaire_form[practice_staff]', with: 'This is an example'
    fill_in 'questionnaire_form[barrier_free_access]', with: 'Lorem ipsum'
    find('input#questionnaire_form_barrier_free_access_options_wheelchair_access',
         visible: false).set(true)
    fill_in 'questionnaire_form[amount_of_visits]', with: 'loads'
    find('input#questionnaire_form_health_insurance_statutory',
         visible: false).set(true)
  end

  def fill_in_step2
    find("input#questionnaire_form_treatment_method_ids_#{@treatment_method.id}",
         visible: false).set(true)
    fill_in 'questionnaire_form[additional_recommended_treatments]',
            with: 'Another treatment'
    fill_in 'questionnaire_form[treatment_rejected]', with: 'This is a comment'
    find('input#questionnaire_form_financing_of_treatments_boolean_yes',
         visible: false).set(true)
    fill_in 'questionnaire_form[payments_required_for_specific_treatments]',
            with: 'Yes, foo bar'
  end

  def fill_in_step3
    find('input#questionnaire_form_listening_boolean_yes', visible: false)
      .set(true)
    fill_in 'questionnaire_form[listening][comment]', with: 'Some comment'
    find('input#questionnaire_form_consent_boolean_yes', visible: false)
      .set(true)
    fill_in 'questionnaire_form[consent][comment]', with: 'Some comment'
    find('input#questionnaire_form_explanation_of_treatment_boolean_no',
         visible: false).set(true)
    fill_in 'questionnaire_form[explanation_of_treatment][comment]',
            with: 'treating person gave no explanation'
    find('input#questionnaire_form_examination_execution_stay_clothed',
         visible: false).set(true)
    find('input#questionnaire_form_treated_with_respect_boolean_yes',
         visible: false).set(true)
    fill_in 'questionnaire_form[treated_with_respect][comment]',
            with: 'Lorem ipsum'
    fill_in 'questionnaire_form[respect_anything_else]', with: 'Some comment'
  end

  def fill_in_step4
    find("input#questionnaire_form_tag_ids_#{@tag1.id}",
         visible: false).set(true)
    find("input#questionnaire_form_tag_ids_#{@tag2.id}",
         visible: false).set(true)
    fill_in 'questionnaire_form[additional_self_describing_tags]',
            with: 'Something else'
    fill_in 'questionnaire_form[limit_recommendation]',
            with: 'I want to limit my recommendation'
  end
end
