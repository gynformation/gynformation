# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Creating tags', type: :feature do
  it 'redirects to tag index on success' do
    user = create(:user, email: 'admin@example.org')
    visit new_admin_tag_path(as: user)

    fill_in 'tag_de', with: 'Trans freundlich'
    fill_in 'tag_en', with: 'Trans friendly'

    click_button I18n.t('admin.tags.form.save')

    expect(page).to have_selector 'td', text: 'Trans friendly'
  end

  it 'requires de and en name' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_tag_path(as: user)

    click_button I18n.t('admin.tags.form.save')

    expect(page).to have_text "Tag name (english) can't be blank"
  end
end
