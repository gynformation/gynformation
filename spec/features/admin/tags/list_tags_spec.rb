# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Listing tags', type: :feature do
  it 'lists all tags' do
    _tag1 = create(:tag, key: 'trans_friendly',
                         name: {
                           en: 'Trans friendly',
                           de: 'Trans freundlich'
                         })
    _tag2 = create(:tag, key: 'sexworker_friendly',
                         name: {
                           en: 'Sex worker friendly',
                           de: 'Sexworkerinnen freundlich'
                         })
    user = create(:user)

    visit admin_tags_path(as: user)

    expect(page).to have_selector(:css, 'table tbody tr', count: 2)
  end
end
