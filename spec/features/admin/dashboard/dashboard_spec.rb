# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Admin Dashboard', type: :feature do
  it 'displays the number of published and unpublished profiles' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    create_list(:published_profile, 3)
    create_list(:unpublished_profile, 4)

    visit admin_dashboard_index_path

    published_profiles_count = find(
      'p[data-test="published-profile-count"]'
    ).text
    un_published_profiles_count = find(
      'p[data-test="unpublished-profile-count"]'
    ).text

    expect(published_profiles_count).to eq '3'
    expect(un_published_profiles_count).to eq 'Unpublished: 4'
  end

  it 'displays the numbers of questionnaires for a status' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    create_list(:new_questionnaire, 3)
    create_list(:check_1_completed_questionnaire, 4)
    create_list(:consultation_required_questionnaire, 2)
    create_list(:inquired_about_abortion_questionnaire, 5)

    visit admin_dashboard_index_path
    new_questionnaires_count = find(
      'p[data-test="new-questionnaires-count"]'
    ).text
    check_1_completed_questionnaire_count = find(
      'p[data-test="check-1-completed-questionnaire-count"]'
    ).text
    consultation_required_questionnaire_count = find(
      'p[data-test="consultation-required-questionnaire-count"]'
    ).text
    inquired_about_abortion_questionnairee_count = find(
      'p[data-test="inquired-about-abortion-questionnaire-count"]'
    ).text

    expect(new_questionnaires_count).to eq 'New: 3'
    expect(check_1_completed_questionnaire_count).to eq 'First check completed: 4'
    expect(consultation_required_questionnaire_count).to eq(
      'Consultation required: 2'
    )
    expect(inquired_about_abortion_questionnairee_count).to eq(
      'Inquired about abortion: 5'
    )
  end
end
