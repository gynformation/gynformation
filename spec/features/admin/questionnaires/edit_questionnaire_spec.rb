# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Editing questionnaires', type: :feature do
  before do
    user = create(:user)
    sign_in_with(user.email, user.password)
  end

  it 'allows admins to add status, comment and profile link' do
    questionnaire = create(:questionnaire)
    visit edit_admin_questionnaire_path(questionnaire.id)

    fill_in 'questionnaire_comment', with: 'Done with the first review'
    select I18n.t('admin.questionnaires.edit.status.check_1_completed'),
           from: 'questionnaire_status'

    click_button I18n.t('admin.questionnaires.edit.save')

    expect(page).to have_content('Done with the first review')
  end

  it 'allows admins to filter existing profiles by last name' do
    create(:profile, last_name: 'Smith', first_name: 'Sandy')
    create(:profile, last_name: 'Meyer', first_name: 'Harriet')
    questionnaire = create(:questionnaire)
    visit edit_admin_questionnaire_path(questionnaire.id)

    fill_in 'last_name', with: 'Smith'
    click_button I18n.t('profile.filter')

    expect(page).to have_content('Sandy Smith')
    expect(page).not_to have_content('Harriet Meyer')
  end

  it 'returns no result when no matching profile was found' do
    create(:profile, last_name: 'Smith', first_name: 'Sandy')
    create(:profile, last_name: 'Meyer', first_name: 'Harriet')
    questionnaire = create(:questionnaire)
    visit edit_admin_questionnaire_path(questionnaire.id)

    fill_in 'last_name', with: 'Snyder'
    click_button I18n.t('profile.filter')

    expect(page).to have_content('No results')
  end

  xit 'allows to link a profile', js: true do
    address = create(:address, city: 'Hamburg')
    create(:profile, last_name: 'Smith', first_name: 'Sandy', address:)
    questionnaire = create(:questionnaire)
    visit edit_admin_questionnaire_path(questionnaire.id)
    fill_in 'last_name', with: 'Smith'
    click_button I18n.t('profile.filter')

    click_link 'link_profile_link'

    expect(page).to have_css('#linked_profile', text: 'Sandy Smith, Hamburg')
  end

  it 'displays the treating persons basic data' do
    answer1 = create(:answer, key: 'first_name', content: 'Lora')
    answer2 = create(:answer, key: 'last_name', content: 'Meier')
    questionnaire = create(:questionnaire, answers: [answer1, answer2])

    visit edit_admin_questionnaire_path(questionnaire.id)

    expect(page).to have_content('Lora Meier')
  end

  it 'formats health insurance answers' do
    answer = create(:answer, key: 'health_insurance',
                             content: ['private', '', 'statutory'])
    questionnaire = create(:questionnaire, answers: [answer])

    visit edit_admin_questionnaire_path(questionnaire.id)

    expect(page).to have_content('private, statutory')
  end

  it 'displays category when category is selected' do
    answer = create(:answer, key: 'category', content: 'endocrinologist')
    questionnaire = create(:questionnaire, answers: [answer])

    visit edit_admin_questionnaire_path(questionnaire.id)

    display_selected_category = find(
      'p[data-test="display-selected-category"]'
    ).text

    expect(display_selected_category).to eq('Endocrinologist')
  end

  it 'displays "-" when no category is selected' do
    questionnaire = create(:questionnaire)
    visit edit_admin_questionnaire_path(questionnaire.id)

    display_selected_category = find(
      'p[data-test="display-selected-category"]'
    ).text

    expect(display_selected_category).to eq('-')
  end
end
