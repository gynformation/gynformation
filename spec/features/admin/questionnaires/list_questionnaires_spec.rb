# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Listing questionnaires', type: :feature do
  it 'lists all questionnaires' do
    answer1 = create(:answer, key: 'first_name', content: 'Hanni')
    answer2 = create(:answer, key: 'last_name', content: 'Example')
    answer3 = create(:answer, key: 'first_name', content: 'Hanni')
    answer4 = create(:answer, key: 'last_name', content: 'Example')
    create(:questionnaire, answers: [answer1, answer2])
    create(:questionnaire, answers: [answer3, answer4])
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    expect(page).to have_selector(:css, 'table tbody tr', count: 2)
  end

  it 'displays the questionnaires status' do
    answer1 = create(:answer, key: 'first_name', content: 'Hanni')
    answer2 = create(:answer, key: 'last_name', content: 'Example')
    create(:questionnaire, status: :consultation_required,
                           answers: [answer1, answer2])
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    expect(page).to have_text('Consultation required')
  end

  it 'links to edit/show questionnaire' do
    questionnaire = create(:questionnaire)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    expect(page).to have_link(
      href: edit_admin_questionnaire_path(questionnaire)
    )
  end

  it 'links to delete questionnaire' do
    questionnaire = create(:questionnaire)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    expect(page).to have_link(
      href: admin_questionnaire_path(questionnaire)
    )
  end

  it 'links to a profile if profile id is present' do
    profile = create(:profile)
    create(:questionnaire, profile:)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    expect(page).to have_link(
      href: admin_profile_path(profile.id)
    )
  end

  it 'filters by status' do
    create(:questionnaire, comment: 'I am a new questionnaire', status: :new)
    create(:questionnaire, comment: 'Consultation required',
                           status: :consultation_required)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    select I18n
      .t('admin.questionnaires.filter_bar.status.consultation_required'),
           from: 'status'
    click_button I18n.t('admin.questionnaires.filter_bar.filter')

    expect(page).not_to have_text('I am a new questionnaire')
    expect(page).to have_text('Consultation required')
  end

  it 'allows to search by last name' do
    answer1 = create(:answer, key: 'last_name', content: 'Smith')
    answer2 = create(:answer, key: 'first_name', content: 'Jenny')
    answer3 = create(:answer, key: 'last_name', content: 'Meyer')
    answer4 = create(:answer, key: 'first_name', content: 'Ellie')
    create(:questionnaire, answers: [answer1, answer2])
    create(:questionnaire, answers: [answer3, answer4])

    user = create(:user)

    visit admin_questionnaires_path(as: user)

    fill_in 'last_name', with: 'Smith'
    click_button I18n.t('admin.questionnaires.filter_bar.filter')

    expect(page).to have_text('Jenny Smith')
    expect(page).not_to have_text('Ellie Meyer')
  end

  it 'displays a pagination for more than 25 results' do
    create_list(:questionnaire, 26)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    expect(page).to have_selector '.pagy-bulma-nav'
  end
end
