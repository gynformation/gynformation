# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Creating treatment methods', type: :feature do
  it 'redirects to treatment method index on success' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_treatment_method_path

    fill_in 'treatment_method_de', with: 'Behandlungsmethode'
    fill_in 'treatment_method_en', with: 'Example Treatment method'

    click_button I18n.t('admin.treatment_methods.form.save')

    expect(page).to have_selector 'td', text: 'Example Treatment method'
  end

  it 'requires de and en name' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_treatment_method_path

    click_button I18n.t('admin.treatment_methods.form.save')

    expect(page).to have_content "Treatment method (english) can't be blank"
  end
end
