# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Updating treatment methods', type: :feature do
  it 'redirects to treatment methods index on success' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    treatment_method = create(:treatment_method)
    visit edit_admin_treatment_method_path(treatment_method.id)

    fill_in 'treatment_method_de', with: 'Neuer Behandlungsmethod Name'
    fill_in 'treatment_method_en', with: 'New treatment method name'

    click_button I18n.t('admin.treatment_methods.form.save')

    expect(page).to have_selector 'td', text: 'New treatment method name'
  end
end
