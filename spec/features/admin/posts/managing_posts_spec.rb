# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Managing posts', type: :feature do
  before do
    @post = create(:post)
    @user = create(:user, email: 'admin@example.org')
  end

  it 'lists all posts' do
    visit admin_posts_url(as: @user)
    expect(page).to have_selector 'h1', text: 'Blog'
  end

  it 'creates posts' do
    visit admin_posts_url(as: @user)
    click_on 'New Post'

    fill_in 'Author', with: 'Käte Frankenthal'
    find(:css, '#post_body').click.set('Lorem ipsum dolor sit amet')
    fill_in 'Teaser', with: 'consetetur sadipscing elitr'
    fill_in 'Title', with: 'Lorem'
    fill_in 'Publish date', with: '07/08/2020'
    attach_file('post_main_image', Rails.root.join('app/assets/images/condoms.png'))
    fill_in 'Main image title', with: 'This is the main image title'
    fill_in 'Main image credit', with: 'This is the main image credit'
    within(:css, '.actions') do
      find('.button.is-primary').click
    end

    expect(page).to have_content 'Post was successfully created'
  end

  it 'shows an error when creating a post without a publish date' do
    visit admin_posts_url(as: @user)
    click_on 'New Post'

    fill_in 'Title', with: 'My blog post title'
    within(:css, '.actions') do
      find('.button.is-primary').click
    end

    expect(page).to have_content "Publish date can't be blank"
  end

  it 'updates a Post' do
    visit admin_posts_url(as: @user)
    find('a[data-test="edit-post-link"]').click

    fill_in 'Author', with: 'Käte Frankenthal'
    find(:css, '#post_body').click.set('Lorem ipsum dolor sit amet')
    fill_in 'Teaser', with: 'consetetur sadipscing elitr'
    fill_in 'Title', with: 'Lorem'
    fill_in 'Publish date', with: '07/08/2020'
    attach_file('post_main_image', Rails.root.join('app/assets/images/condoms.png'))
    fill_in 'Main image title', with: 'This is the main image title'
    fill_in 'Main image credit', with: 'This is the main image credit'
    check 'post_published'
    within(:css, '.actions') do
      find('.button.is-primary').click
    end

    expect(page.find('image')['src']).to have_content 'condoms.png'
    expect(page).to have_content 'Post was successfully updated'
  end

  it 'destroys a Post' do
    visit admin_posts_url(as: @user)

    find('a[data-test="delete-post-link"]').click

    expect(page).to have_content 'Post was successfully destroyed'
  end
end
