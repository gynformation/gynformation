# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Updating profiles', type: :feature do
  before do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    create(:tag, name: { en: 'Fat friendly', de: 'Fat freundlich' })
    create(:treatment_method, name: {
             en: 'Abortion', de: 'Schwangerschaftsabbruch'
           })
    @profile = create(:published_profile)
    @address = create(:address, profile_id: @profile.id)
    @comment = create(:comment, profile_id: @profile.id)
  end

  it 'redirects to the created profile on success' do
    visit admin_profile_url(@profile.id)
    click_on I18n.t('profile.edit')

    fill_in 'profile_last_name', with: 'Dora'
    fill_in 'profile_first_name', with: 'The Explorer'
    fill_in 'profile_address_attributes_street', with: 'Update Street'
    fill_in 'profile_address_attributes_zip', with: '88888'
    fill_in 'profile_address_attributes_city', with: 'Hamburg'
    select 'Hamburg', from: 'profile_address_attributes_state'
    fill_in 'profile_website_with_protocol', with: 'https://www.updated-website.org'
    fill_in 'profile_phone_number', with: '1111111111111111111'
    fill_in 'profile_accessibility_addition', with: 'Updated information about
     accessibility'
    fill_in 'profile_language', with: 'Another language'
    find('input#profile_health_insurance_statutory',
         visible: false).set(true)
    fill_in 'profile_financing_of_treatments', with: 'Updated info about
    financing'
    fill_in 'profile_comments_attributes_0_content', with: 'This is an updated comment'
    select 'Fat friendly', from: 'profile_tag_ids', visible: false
    select 'Abortion', from: 'profile_treatment_method_ids', visible: false

    click_button I18n.t('profile.save')

    expect(page).to have_text('Dora')
    expect(page).to have_text('The Explorer')
    expect(page).to have_text('Update Street')
    expect(page).to have_text('88888')
    expect(page).to have_text('Hamburg')
    expect(page).to have_text('https://www.updated-website.org')
    expect(page).to have_text('1111111111111111111')
    expect(page).to have_text('Updated information about accessibility',
                              normalize_ws: true)
    expect(page).to have_text('Another language')
    expect(page).to have_text(I18n.t('questionnaire_form.statutory'))
    expect(page).to have_text('Updated info about financing',
                              normalize_ws: true)
    expect(page).to have_text('This is an updated comment', normalize_ws: true)
    expect(page).to have_text('Fat friendly')
    expect(page).to have_text('Profile updated')
  end

  xit 'allows to add a second comment input field', js: true do
    visit edit_admin_profile_path(@profile.id)

    click_on 'Kommentar hinzufügen'

    comment_input_fields = page.all('textarea[id^="profile_comments_attributes"]')
    expect(comment_input_fields.length).to eq 2
  end

  it 'update an unpublished profile' do
    unpublished_profile = create(:unpublished_profile)
    visit admin_profile_url(unpublished_profile)
    click_on I18n.t('profile.edit')

    fill_in 'profile_last_name', with: 'Updated Name'
    click_button I18n.t('profile.save')

    expect(page).to have_text('Updated Name')
    expect(page).to have_text('Profile updated')
  end

  it 'displays an error message when name is missing' do
    visit admin_profile_url(@profile.id)
    click_on I18n.t('profile.edit')

    fill_in 'profile_last_name', with: ''
    click_button I18n.t('profile.save')

    expect(page).to have_content(
      I18n.t('activerecord.errors.models.profile.attributes.last_name.blank')
    )
  end
end
