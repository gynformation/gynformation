# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Creating profiles', type: :feature do
  before do
    user = create(:user)
    sign_in_with(user.email, user.password)
  end

  it 'redirects to created profile on success' do
    create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    create(:tag, name: { en: 'Fat friendly', de: 'Fat freundlich' })
    create(:treatment_method,
           name: { en: 'Abortion', de: 'Schwangerschaftsabbruch' })
    visit new_admin_profile_path

    fill_form
    check 'profile_published'
    click_button I18n.t('profile.create')

    expect(page).to have_text('Lora')
    expect(page).to have_text('Ipsum')
    expect(page).to have_text('https://www.foobar.org')
    expect(page).to have_text('This is a comment')
    expect(page).to have_text('English')
    expect(page).to have_text('Example Street')
    expect(page).to have_text('12345')
    expect(page).to have_text('Berlin')
    expect(page).to have_text('Trans friendly')
    expect(page).to have_text('This is a comment')
    expect(page).to have_text('An internal comment')
    expect(page).not_to have_button(I18n.t('profile.create'))
  end

  it 'redirects to created profile in case of an unpublished profile' do
    visit new_admin_profile_path

    fill_in 'profile_first_name', with: 'Lora'
    fill_in 'profile_last_name', with: 'Meier'
    select 'Berlin', from: 'profile_address_attributes_state'
    select 'Gynecologist', from: 'profile_category'

    click_button I18n.t('profile.create')

    expect(page).to have_text('Lora Meier')
  end

  it 'displays an error message when name is missing' do
    visit new_admin_profile_path

    fill_in 'profile_website_with_protocol', with: 'https://www.foobar.org'
    click_button I18n.t('profile.create')

    expect(page).to have_content(
      I18n.t('activerecord.errors.models.profile.attributes.last_name.blank')
    )
  end

  it 'displays an error message when category is missing' do
    visit new_admin_profile_path

    fill_in 'profile_website_with_protocol', with: 'https://www.foobar.org'
    click_button I18n.t('profile.create')

    expect(page).to have_content(
      I18n.t('activerecord.errors.models.profile.attributes.category.blank')
    )
  end

  it 'displays an error message when URL has wrong format' do
    visit new_admin_profile_path

    fill_in 'profile_first_name', with: 'Lora'
    fill_in 'profile_last_name', with: 'Meier'
    select 'Berlin', from: 'profile_address_attributes_state'
    select 'Gynecologist', from: 'profile_category'

    fill_in 'profile_website_with_protocol', with: 'www.foobar.org'
    click_button I18n.t('profile.create')

    expect(page).to have_content(
      I18n.t('activerecord.errors.models.profile.attributes.website_with_protocol.wrong_format')
    )
  end

  it 'prefills answers when creating new profile from questionnaire' do
    answer1 = create(:answer, key: 'first_name', content: 'Lora')
    answer2 = create(:answer, key: 'last_name', content: 'Meier')
    answer3 = create(:answer, key: 'category', content: 'endocrinologist')
    answer4 = create(:answer, key: 'state', content: 'DE-HH')
    @questionnaire = create(:questionnaire, answers: [answer1, answer2, answer3, answer4])

    @treatment_method = create(:treatment_method, name: {
                                 en: 'Abortion', de: 'Schwangerschaftsabbruch'
                               })
    create(:questionnaire_treatment, treatment_method_id: @treatment_method.id, questionnaire_id: @questionnaire.id)

    @tag = create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    create(:questionnaire_tagging, tag_id: @tag.id, questionnaire_id: @questionnaire.id)

    visit new_admin_profile_path(questionnaire_id: @questionnaire.id)

    expect(page).to have_text('Lora Meier')
    expect(page).to have_text('Trans friendly')
    expect(page).to have_text('Abortion')
  end

  def fill_form
    fill_in 'profile_first_name', with: 'Lora'
    fill_in 'profile_last_name', with: 'Ipsum'
    fill_in 'profile_address_attributes_street', with: 'Example Street'
    fill_in 'profile_address_attributes_zip', with: '12345'
    fill_in 'profile_address_attributes_city', with: 'Berlin'
    select 'Berlin', from: 'profile_address_attributes_state'
    fill_in 'profile_website_with_protocol', with: 'https://www.foobar.org'
    fill_in 'profile_phone_number', with: '040/2123214300923'
    select 'Gynecologist', from: 'profile_category'
    find('input#profile_accessibility_options_sign_language',
         visible: false).set(true)
    fill_in 'profile_accessibility_addition', with: 'Some information'
    fill_in 'profile_language', with: 'English'
    find('input#profile_health_insurance_statutory',
         visible: false).set(true)
    fill_in 'profile_financing_of_treatments', with: 'Some content'
    click_on 'Kommentar hinzufügen'
    fill_in 'profile_comments_attributes_0_content', with: 'This is a comment'
    fill_in 'profile_internal_comment', with: 'An internal comment'
    select 'Trans friendly', from: 'profile_tag_ids', visible: false
    select 'Abortion', from: 'profile_treatment_method_ids', visible: false
  end
end
