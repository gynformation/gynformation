# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Listing profiles', type: :feature do
  before do
    user = create(:user)
    sign_in_with(user.email, user.password)
  end

  it 'lists all profiles' do
    create(:published_profile, last_name: 'First Profile')
    create(:unpublished_profile, last_name: 'Second Profile')

    visit admin_profiles_path

    expect(page).to have_text('First Profile')
    expect(page).to have_text('Second Profile')
  end

  it 'filtering profiles by published state' do
    create(:published_profile, last_name: 'I am a published profile')
    create(:unpublished_profile, last_name: 'Hey, I am unpublished')

    visit admin_profiles_path

    select I18n.t('profile.published.false'), from: 'published'
    click_button I18n.t('profile.filter')

    expect(page).not_to have_text('I am a published profile')
    expect(page).to have_text('Hey, I am unpublished')
  end

  it 'searching for profiles by last name' do
    create(:profile, last_name: 'Schmidt')
    create(:profile, last_name: 'Meier')

    visit admin_profiles_path

    fill_in 'last_name', with: 'Meier'
    click_button I18n.t('profile.filter')

    expect(page).not_to have_text('Schmidt')
    expect(page).to have_text('Meier')
  end

  it 'profiles list entry links to edit profile' do
    profile = create(:profile)

    visit admin_profiles_path

    expect(page).to have_link(href: edit_admin_profile_path(profile))
  end

  it 'links to create profile' do
    visit admin_profiles_path

    expect(page).to have_link(I18n.t('admin.new_profile'),
                              href: new_admin_profile_path)
  end

  it 'links to delete profile' do
    profile = create(:profile)

    visit admin_profiles_path

    expect(page).to have_link(href: admin_profile_path(profile))
  end

  it 'links to show profile' do
    profile = create(:profile)

    visit admin_profiles_path

    expect(page).to have_link(href: admin_profile_path(profile))
  end

  it 'displays a pagination for more than 25 results' do
    create_list(:profile, 26)
    user = create(:user)

    visit admin_profiles_path(as: user)

    expect(page).to have_selector '.pagy-bulma-nav'
  end
end
