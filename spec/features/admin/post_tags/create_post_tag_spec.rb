# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Creating post tags', type: :feature do
  it 'redirects to post tag index on success' do
    user = create(:user, email: 'admin@example.org')
    visit new_admin_post_tag_path(as: user)

    fill_in 'post_tag_de', with: 'Neuigkeiten'
    fill_in 'post_tag_en', with: 'News'

    click_button I18n.t('admin.post_tags.form.save')

    expect(page).to have_selector 'td', text: 'News'
  end

  it 'requires de and en name' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_post_tag_path(as: user)

    click_button I18n.t('admin.post_tags.form.save')

    expect(page).to have_text "En can't be blank"
  end
end
