# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Updating post tags', type: :feature do
  it 'redirects to post tag index on success' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    post_tag = create(:post_tag)
    visit edit_admin_post_tag_path(post_tag.id)

    fill_in 'post_tag_de', with: 'Neuer Tagname'
    fill_in 'post_tag_en', with: 'New tag name'

    click_button I18n.t('admin.post_tags.form.save')

    expect(page).to have_selector 'td', text: 'New tag name'
  end
end
