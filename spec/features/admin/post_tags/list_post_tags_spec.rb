# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Listing post tags', type: :feature do
  it 'lists all post tags' do
    _tag1 = create(:post_tag, key: 'news',
                              name: {
                                en: 'Neuigkeiten',
                                de: 'Neuigkeiten'
                              })
    _tag2 = create(:post_tag, key: 'history',
                              name: {
                                en: 'History',
                                de: 'Geschichte'
                              })
    user = create(:user)

    visit admin_post_tags_path(as: user)

    expect(page).to have_selector(:css, 'table tbody tr', count: 2)
  end
end
