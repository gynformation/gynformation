# frozen_string_literal: true

require 'rails_helper'
RSpec.describe 'Navigation bar', type: :feature do
  it 'has all the menu links' do
    visit root_url

    expect(page).to have_link I18n.t('navigation.search'), href: profiles_path
    expect(page).to have_link I18n.t('navigation.submit'),
                              href: new_questionnaire_path
    expect(page).to have_link I18n.t('navigation.blog'), href: posts_path
  end

  it 'allows users to switch the locale' do
    visit root_url
    expect(page).to have_current_path('/en')

    click_link 'Deutsch'

    expect(page).to have_current_path('/de')
  end
end
