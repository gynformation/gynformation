# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Listing posts', type: :feature do
  before do
    @post1 = create(:published_post)
    @post1_tag = create(:post_tag, key: 'news', name: { en: 'News', de: 'Neuigkeiten' })
    create(:post_tagging, post_tag_id: @post1_tag.id, post_id: @post1.id)

    @post2 = create(:published_post)
    @post2_tag = create(:post_tag, key: 'history', name: { en: 'History', de: 'Geschichte' })
    create(:post_tagging, post_tag_id: @post2_tag.id, post_id: @post2.id)
  end

  it 'visiting the index' do
    visit posts_url

    expect(page).to have_css 'ul', class: 'post-list'
  end

  it 'displays tags, title, author and teaser ' do
    visit posts_url

    expect(page).to have_content 'News'
    expect(page).to have_content @post1.title
    expect(page).to have_content @post1.teaser
    expect(page).to have_content @post1.author
  end

  it 'filters posts by post tag' do
    visit posts_url

    click_link 'News'
    expect(page).to have_content 'News'
    expect(page).not_to have_content 'History'
  end

  describe 'when locale is english' do
    it 'displays an info box' do
      visit posts_url(locale: :en)

      expect(page).to have_css 'div', class: 'info-box'
    end
  end

  describe 'when locale is german' do
    it 'does not display an info box' do
      visit posts_url(locale: :en)

      expect(page).to have_css 'div', class: 'info-box'
    end
  end
end
