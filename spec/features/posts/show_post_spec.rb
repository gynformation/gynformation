# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Showing posts', type: :feature do
  before do
    @post1 = create(:published_post, title: 'Title of the blog post')
    @post1_tag = create(:post_tag, key: 'news', name: { en: 'News', de: 'Neuigkeiten' })
    create(:post_tagging, post_tag_id: @post1_tag.id, post_id: @post1.id)

    @post2 = create(:published_post, title: 'Title of the second blog post')
    @post2_tag = create(:post_tag, key: 'history', name: { en: 'History', de: 'Geschichte' })
    create(:post_tagging, post_tag_id: @post2_tag.id, post_id: @post2.id)
  end

  it 'displays title, author, teaser and body' do
    visit post_url(@post1.id)

    expect(page).to have_content 'News'
    expect(page).to have_content @post1.title
    expect(page).to have_content @post1.author
    expect(page).to have_content 'Title of the blog post'
  end

  it 'returns to post list and filters posts by post tag' do
    visit post_url(@post1.id)

    click_link 'News'
    expect(page).to have_content 'News'
    expect(page).to have_content @post1.title
    expect(page).to have_content @post1.author
    expect(page).to have_content 'Title of the blog post'
    expect(page).not_to have_content 'History'
    expect(page).not_to have_content 'Title of the second blog post'
  end
end
