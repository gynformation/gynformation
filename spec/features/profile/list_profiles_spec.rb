# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Listing posts', type: :feature do
  before do
    @profile = create(
      :published_profile,
      first_name: 'Lora',
      last_name: 'Ipsum',
      website_with_protocol: 'https://www.example.org'
    )
    @address = create(:address, state: 'DE-BB', profile_id: @profile.id)
    @tag = create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    create(:tagging, tag_id: @tag.id, profile_id: @profile.id)
    @treatment_method = create(:treatment_method,
                               name: {
                                 en: 'Sterilization', de: 'Sterilisation'
                               }, key: 'sterilization')
    create(:profile_treatment, treatment_method_id: @treatment_method.id,
                               profile_id: @profile.id)
  end

  it 'visiting the index' do
    visit profiles_url

    expect(page).to have_css 'ul', class: 'profile__list'
  end

  it 'displays the tags ' do
    visit profiles_url

    expect(page).to have_content 'Trans friendly'
  end

  it 'displays the treatment methods ' do
    visit profiles_url

    expect(page).to have_content 'Sterilization'
  end

  it 'displays the language' do
    visit profiles_url

    expect(page).to have_content @profile.language
  end

  it 'displays the adress' do
    visit profiles_url

    expect(page).to have_content 'Bakerstreet 121, 20148 Hamburg, Brandenburg'
  end

  it 'links to profile from index' do
    visit profiles_url

    find("a[href='#{profile_path(@profile.id)}']").click

    expect(page).to have_content 'https://www.example.org'
  end

  it 'filtering profiles by tag' do
    skip('No idea, why this is failing, skipping for now')
    second_profile = create(
      :published_profile,
      last_name: 'Second Profile'
    )
    second_tag = create(:tag,
                        name: { en: 'Queer friendly', de: 'Queer freundlich' })
    create(:tagging, tag_id: second_tag.id, profile_id: second_profile.id)

    visit profiles_url

    select 'Queer friendly', from: 'tag'
    click_button I18n.t('profile.filter')

    expect(page).to have_content 'Second Profile'
    expect(page).not_to have_content 'Ipsum'
  end

  it 'filtering profiles by treatment method' do
    second_profile = create(
      :published_profile,
      last_name: 'Profile with selected treatment method'
    )
    second_treatment_method = create(:treatment_method,
                                     name: { en: 'Abortion',
                                             de: 'Schwangerschaftsabbruch' })
    create(:profile_treatment,
           treatment_method_id: second_treatment_method.id,
           profile_id: second_profile.id)
    visit profiles_url

    select 'Abortion', from: 'treatment_method'
    click_button I18n.t('profile.filter')

    expect(page).to have_content 'Profile with selected treatment method'
    expect(page).not_to have_content 'Ipsum'
  end

  it 'filtering profiles by state' do
    second_profile = create(
      :profile,
      last_name: 'Second Profile'
    )
    create(
      :address,
      state: 'DE-HH',
      profile_id: second_profile.id
    )
    visit profiles_url

    select 'Brandenburg', from: 'state'
    click_button I18n.t('profile.filter')

    expect(page).to have_content 'Ipsum'
    expect(page).not_to have_content 'Second Profile'
  end

  it 'displays a pagination for more than 25 results' do
    create_list(:profile, 26)
    user = create(:user)

    visit admin_profiles_path(as: user)

    expect(page).to have_css '.pagy-bulma-nav'
  end

  context 'show linked questionnaires as number of recommendations with correct pluralization' do
    it 'displays 0 recommendations' do
      visit profiles_url

      expect(page).to have_content '0 recommendations'
    end

    it 'displays 1 recommendation' do
      second_profile = create(
        :published_profile,
        last_name: 'Second Profile'
      )
      create(
        :questionnaire,
        profile_id: second_profile.id
      )

      visit profiles_url
      expect(page).to have_content '1 recommendation'
    end

    it 'displays 2 recommendations' do
      second_profile = create(
        :published_profile,
        last_name: 'Second Profile'
      )
      create(
        :questionnaire,
        profile_id: second_profile.id
      )
      create(
        :questionnaire,
        profile_id: second_profile.id
      )

      visit profiles_url
      expect(page).to have_content '2 recommendations'
    end
  end
end
