# frozen_string_literal: true

require 'rails_helper'
require 'support/features/clearance_helpers'

RSpec.describe 'Post detail view', type: :feature do
  before do
    @profile = create(:published_profile)
    @address = create(:address, profile_id: @profile.id, state: 'DE-HH')
    create(:comment, content: 'A comment', profile_id: @profile.id)
    create(:comment, content: 'A second comment', profile_id: @profile.id)
    @tag = create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    @treatment_method = create(:treatment_method, name: {
                                 en: 'Abortion', de: 'Schwangerschaftsabbruch'
                               })
    create(:profile_treatment, treatment_method_id: @treatment_method.id,
                               profile_id: @profile.id)
    create(:tagging, tag_id: @tag.id, profile_id: @profile.id)
  end

  it 'displays the profile data' do
    visit profile_url(@profile.id)

    expect(page).to have_content @profile.first_name
    expect(page).to have_content @profile.last_name
    expect(page).to have_content @profile.website_with_protocol
    expect(page).to have_content 'A comment'
    expect(page).to have_content 'A second comment'
    expect(page).to have_content @profile.language
    expect(page).to have_content 'General Practicioner'
    expect(page).to have_content @profile.phone_number
    expect(page).to have_content @profile.financing_of_treatments
    expect(page).to have_content @profile.accessibility_addition
  end

  it 'displays the address' do
    visit profile_url(@profile.id)

    expect(page).to have_content @address.street
    expect(page).to have_content @address.zip
    expect(page).to have_content @address.city
    expect(page).to have_content 'Hamburg'
  end

  it 'displays the tags ' do
    visit profile_url(@profile.id)

    expect(page).to have_content 'Trans friendly'
  end

  it 'displays the associated treatment methods ' do
    visit profile_url(@profile.id)

    expect(page).to have_content 'Abortion'
  end

  it 'displays last updated date' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)

    visit profile_url(@profile.id)

    expect(page).to have_content(
      "Last updated: #{I18n.l(@profile.updated_at, format: :short)}"
    )
  end

  context 'when user is signed in' do
    it 'displays an edit button' do
      user = create(:user, email: 'admin@example.org')
      sign_in_with(user.email, user.password)

      visit profile_url(@profile.id)

      expect(page).to have_link I18n.t('profile.edit')
    end

    it 'displays internal comments' do
      user = create(:user, email: 'admin@example.org')
      sign_in_with(user.email, user.password)
      profile = create(:profile,
                       internal_comment: 'This is an internal comment',
                       published: true)

      visit profile_url(profile.id)

      expect(page).to have_content 'This is an internal comment'
    end

    it 'links to associated questionnaires' do
      user = create(:user, email: 'admin@example.org')
      profile = create(:profile,
                       internal_comment: 'This is an internal comment',
                       published: true)
      sign_in_with(user.email, user.password)
      questionnaire1 = create(:questionnaire, profile:)
      questionnaire2 = create(:questionnaire, profile:)

      visit profile_url(profile.id)

      expect(page).to have_link(
        href: edit_admin_questionnaire_path(questionnaire1)
      )
      expect(page).to have_link(
        href: edit_admin_questionnaire_path(questionnaire2)
      )
    end
  end

  context 'when user is not signed in' do
    it 'does not display internal comments' do
      profile = create(:profile,
                       internal_comment: 'This is an internal comment',
                       published: true)

      visit profile_url(profile.id)

      expect(page).not_to have_content 'This is an internal comment'
    end

    it 'does not display an edit button' do
      visit profile_url(@profile.id)

      expect(page).not_to have_link I18n.t('profile.edit')
    end

    it 'does not link to associated questionnaires' do
      profile = create(:profile,
                       internal_comment: 'This is an internal comment',
                       published: true)
      questionnaire = create(:questionnaire, profile:)

      visit profile_url(profile.id)

      expect(page).not_to have_link(
        href: edit_admin_questionnaire_path(questionnaire)
      )
    end
  end

  it 'shows number of linked questionnaires as recommendations' do
    create(:questionnaire, profile_id: @profile.id)

    visit profile_url(@profile.id)

    expect(page).to have_content '1 recommendation'
  end
end
