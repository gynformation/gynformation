# frozen_string_literal: true

require('rails_helper')

RSpec.describe Admin::PostTagsController do
  before do
    user = create(:user)
    sign_in_as(user)
  end

  describe 'GET index' do
    it('returns a succes response') do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET new' do
    it('returns a succes response') do
      get :new
      expect(response).to be_successful
    end
  end

  describe 'POST create' do
    it('returns a succes response') do
      user = create(:user)
      sign_in_as(user)
      expect do
        post :create, params: { 'post_tag' =>
          { 'en' => 'News',
            'de' => 'Neuigkeiten' } }
      end.to(change { PostTag.count }.by(1))
      expect(response).to redirect_to(admin_post_tags_path)
    end

    it('saves the key when creating post tag') do
      user = create(:user)
      sign_in_as(user)

      post :create, params: { post_tag: {
        'en' => 'News', 'de' => 'Neuigkeiten'
      } }

      created_tag = PostTag.last
      created_tag.reload
      expect('news').to(eq(created_tag.key))
    end
  end

  describe 'PATCH edit' do
    it('updates the tag') do
      user = create(:user)
      sign_in_as(user)
      post_tag = create(:post_tag)
      patch :update, params: {
        id: post_tag.to_param,
        post_tag: { 'en' => 'New en post tag name', 'de' => 'New de post tag name' }
      }
      updated_tag = PostTag.last
      updated_tag.reload
      expect(updated_tag.en).to eq('New en post tag name')
    end
  end
end
