# frozen_string_literal: true

require('rails_helper')

RSpec.describe Admin::TreatmentMethodsController do
  before do
    user = create(:user)
    sign_in_as(user)
  end

  describe 'GET index' do
    it 'returns a succes response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET new' do
    it 'returns a succes response' do
      get :new
      expect(response).to be_successful
    end
  end

  describe 'POST create' do
    it 'creates a treatment_method' do
      expect do
        post :create, params: { 'treatment_method' =>
          { 'en' => 'English Treatment method',
            'de' => 'German Treatment method' } }
      end.to change { TreatmentMethod.count }.by(1)
      expect(response).to redirect_to(admin_treatment_methods_path)
    end

    it 'saves key when creating treatment_method' do
      user = create(:user)
      sign_in_as(user)
      post :create, params: { 'treatment_method' =>
        { 'en' => 'English Treatment method',
          'de' => 'German Treatment method' } }
      created_treatment_method = TreatmentMethod.last
      created_treatment_method.reload
      expect(created_treatment_method.key).to eq('english_treatment_method')
    end
  end

  describe 'PATCH #update' do
    it('updates the treatment_method') do
      user = create(:user)
      sign_in_as(user)
      treatment_method = create(:treatment_method)
      patch :update, params: {
        id: treatment_method.id,
        'treatment_method' =>
        { 'en' => 'New en treatment_method name',
          'de' => 'New de treatment_method name' }
      }
      updated_treatment_method = TreatmentMethod.last
      updated_treatment_method.reload
      expect(updated_treatment_method.en).to eq('New en treatment_method name')
    end

    it 'does not update the key' do
      user = create(:user)
      sign_in_as(user)
      treatment_method = create(:treatment_method, key: 'old key')

      patch :update, params: {
        id: treatment_method.to_param,
        treatment_method: { 'en' => 'New en treatment_method name', 'de' => 'New de treatment_method name' }
      }

      updated_treatment_method = TreatmentMethod.last
      updated_treatment_method.reload
      expect(updated_treatment_method.key).to eq('old key')
    end
  end
end
