# frozen_string_literal: true

require('rails_helper')

RSpec.describe Admin::QuestionnairesController do
  before do
    user = create(:user)
    sign_in_as(user)
  end

  describe 'GET index' do
    it('returns a succes response') do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET edit' do
    it('returns a succes response') do
      questionnaire = create(:questionnaire)

      get :edit, params: { id: questionnaire.id }

      expect(response).to be_successful
    end
  end

  describe 'PATCH #update' do
    it('updates the questionaire') do
      questionnaire = create(:questionnaire,
                             comment: 'Some comment', status: 'new')
      new_status = 'profile_published'
      new_comment = 'Something new'

      patch :update, params: {
        id: questionnaire.to_param,
        questionnaire: {
          comment: new_comment,
          status: new_status
        }
      }

      expect(flash[:success]).to match 'Questionnaire updated'
      expect(response).to redirect_to(
        edit_admin_questionnaire_path(questionnaire)
      )
      questionnaire.reload
      expect(questionnaire.status).to(eq(new_status))
      expect(questionnaire.comment).to(eq(new_comment))
    end
  end

  describe 'DELETE destroy' do
    it('deletes the questionnaire') do
      answer = create(:answer)
      questionnaire = create(:questionnaire, answers: [answer])
      expect do
        expect do
          delete :destroy, params: { id: questionnaire.to_param }
        end.to(change { Answer.count }.by(-1))
      end.to(change { Questionnaire.count }.by(-1))
      expect(response).to redirect_to(admin_questionnaires_path)
    end
  end
end
