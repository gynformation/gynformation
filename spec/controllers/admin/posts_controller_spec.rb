# frozen_string_literal: true

require('rails_helper')

RSpec.describe Admin::PostsController do
  before do
    @post = create(:post)
    sign_in_as_user
  end

  describe 'GET index' do
    it('returns a succes response') do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET new' do
    it('returns a succes response') do
      get :new
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it('returns a succes response') do
      get :show, params: { id: @post.to_param }
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it('returns a succes response') do
      get :edit, params: { id: @post.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    it('creates a post') do
      expect do
        post :create, params: { post:
          {
            author: @post.author,
            body: @post.body,
            teaser: @post.teaser,
            title: @post.title,
            publish_date: Date.tomorrow
          } }
      end.to(change { Post.count })

      expect(response).to redirect_to(admin_post_path(Post.first))
    end
  end

  describe 'PATCH #update' do
    it('updates the post') do
      patch :update, params: {
        id: @post.to_param,
        post: {
          author: @post.author,
          body: @post.body,
          teaser: @post.teaser,
          title: @post.title,
          publish_date: @post.publish_date
        }
      }
      expect(response).to redirect_to(admin_post_path(@post))
    end
  end

  describe 'DELETE #destroy' do
    it('destroys the post and redirects') do
      expect do
        delete :destroy, params: { id: @post.to_param }
      end.to(change { Post.count }.by(-1))
      expect(response).to redirect_to(admin_posts_path)
    end
  end

  def sign_in_as_user
    user = create(:user)
    sign_in_as(user)
  end
end
