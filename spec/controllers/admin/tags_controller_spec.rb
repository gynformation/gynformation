# frozen_string_literal: true

require('rails_helper')

RSpec.describe Admin::TagsController do
  before do
    user = create(:user)
    sign_in_as(user)
  end

  describe 'GET index' do
    it('returns a succes response') do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET new' do
    it('returns a succes response') do
      get :new
      expect(response).to be_successful
    end
  end

  describe 'POST create' do
    it('returns a succes response') do
      user = create(:user)
      sign_in_as(user)
      expect do
        post :create, params: { 'tag' =>
          { 'en' => 'Sex worker friendly',
            'de' => 'Sexworkerinnen freundlich' } }
      end.to(change { Tag.count }.by(1))
      expect(response).to redirect_to(admin_tags_path)
    end

    it('saves the key when creating tag') do
      user = create(:user)
      sign_in_as(user)

      post :create, params: { tag: {
        'en' => 'Sex worker friendly', 'de' => 'Sexworkerinnen freundlich'
      } }

      created_tag = Tag.last
      created_tag.reload
      expect('sex_worker_friendly').to(eq(created_tag.key))
    end
  end

  describe 'PATCH edit' do
    it('updates the tag') do
      user = create(:user)
      sign_in_as(user)
      tag = create(:tag)
      patch :update, params: {
        id: tag.to_param,
        tag: { 'en' => 'New en tag name', 'de' => 'New de tag name' }
      }
      updated_tag = Tag.last
      updated_tag.reload
      expect(updated_tag.en).to eq('New en tag name')
    end

    it 'does not update the key' do
      user = create(:user)
      sign_in_as(user)
      tag = create(:tag, key: 'old key')

      patch :update, params: {
        id: tag.to_param,
        tag: { 'en' => 'New en tag name', 'de' => 'New de tag name' }
      }

      updated_tag = Tag.last
      updated_tag.reload
      expect(updated_tag.key).to eq('old key')
    end
  end
end
