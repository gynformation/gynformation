# frozen_string_literal: true

require('rails_helper')

RSpec.describe Admin::DashboardController do
  describe 'GET index' do
    it 'returns a success response' do
      sign_in_as_user

      get :index

      expect(response).to be_successful
    end

    def sign_in_as_user
      user = create(:user)
      sign_in_as(user)
    end
  end
end
