# frozen_string_literal: true

require('rails_helper')

RSpec.describe StaticPagesController do
  it('gets home') do
    get :home
    expect(response).to be_successful
  end
end
