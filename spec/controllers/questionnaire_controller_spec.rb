# frozen_string_literal: true

require('rails_helper')

RSpec.describe QuestionnaireController do
  describe 'GET new' do
    it('returns a succes response') do
      get :new
      expect(response).to be_successful
    end
  end

  describe 'GET info' do
    it('returns a succes response') do
      get :info
      expect(response).to be_successful
    end
  end

  describe 'POST create' do
    it('creates a questionnaire and answers') do
      expect do
        expect do
          post :create, params: {
            questionnaire_form: { first_name: 'Lora', body: 'Ipsum' }
          }
        end.to redirect_to(questionnaire_info_path)
      end
    end

    it('redirects to the info page') do
      expect do
        expect do
          post :create, params: {
            questionnaire_form: { first_name: 'Lora', body: 'Ipsum' }
          }
        end.to redirect_to(questionnaire_info_path)
      end
    end
  end
end
