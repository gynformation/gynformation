# frozen_string_literal: true

require('rails_helper')

RSpec.describe ApplicationController do
  it 'includes HttpAuthConcern' do
    expect(
      ApplicationController.ancestors.include?(HttpAuthConcern)
    ).to eq(true)
  end
end
