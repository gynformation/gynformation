# frozen_string_literal: true

require('rails_helper')

RSpec.describe PostsController do
  before do
    @post = create(:published_post, publish_date: 1.day.ago)
  end

  describe 'GET index' do
    it('returns a succes response') do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it('returns a succes response') do
      get :show, params: { id: @post.to_param }
      expect(response).to be_successful
    end
  end
end
