# frozen_string_literal: true

require 'spec_helper'
require 'rake'

ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../config/environment', __dir__)

abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'rspec/rails'
require 'clearance/rspec'
require 'selenium-webdriver'
require 'webdrivers'

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  config.fixture_path = Rails.root.join('/spec/fixtures')

  config.use_transactional_fixtures = true

  config.infer_spec_type_from_file_location!

  config.filter_rails_from_backtrace!

  config.before(:suite) do
    Comfy::Cms::Site.destroy_all
    Comfy::Cms::Site.create!(identifier: 'gynformation', hostname: 'localhost')
    Rails.application.load_tasks
    Rake::Task['comfy:cms_seeds:import'].invoke('gynformation', 'gynformation')
  end

  config.before(:each, type: :feature) do
    default_url_options[:locale] = I18n.default_locale
  end

  config.before(:each, type: :request) do
    default_url_options[:locale] = I18n.default_locale
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.register_driver :headless_chrome do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: { args: %w[--no-sandbox --headless --disable-gpu
                              --disable-extensions --disable-dev-shm-usage] }
  )

  Capybara::Selenium::Driver.new app,
                                 browser: :chrome,
                                 desired_capabilities: capabilities
end

Capybara.javascript_driver = :headless_chrome
