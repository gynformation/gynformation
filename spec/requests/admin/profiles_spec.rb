# frozen_string_literal: true

require('rails_helper')

describe 'Admin Profiles' do
  before do
    @user = create(:user)
  end

  describe 'GET new' do
    it 'returns a succes response when user is signed in' do
      get new_admin_profile_path(as: @user)

      expect(response).to be_successful
    end

    it 'returns a succes response for a profile created from new questionnaire when user is signed in' do
      get new_admin_profile_path(as: @user, params: { questionnaire_id: create(:questionnaire).id })

      expect(response).to be_successful
    end

    it 'returns a routing error on get /new for a visitor' do
      expect do
        get new_admin_profile_path
      end.to raise_error(ActionController::RoutingError)
    end
  end

  describe 'POST #create' do
    it('creates a profile') do
      expect do
        post admin_profiles_path(as: @user), params: { profile: {
          first_name: 'Lora',
          last_name: 'Ipsum',
          website_with_protocol: 'https://www.example.org',
          category: :midwife
        } }
      end.to(change { Profile.count }.by(1))
      expect(response).to redirect_to(admin_profile_path(Profile.last))
    end

    it('returns a routing error on /create for a visitor') do
      expect do
        post admin_profiles_path, params: { profile: {
          first_name: 'Lora',
          last_name: 'Ipsum',
          website_with_protocol: 'https://www.example.org',
          category: :midwife
        } }
      end.to(raise_error(ActionController::RoutingError))
    end

    it('does not create a profile without last name') do
      expect do
        post admin_profiles_path(as: @user), params: { profile: {
          first_name: 'Lora',
          website_with_protocol: 'https://www.example.org',
          category: :midwife
        } }
      end.to(change { Profile.count }.by(0))
      expect(response.body).to include('Please provide a last name')
    end
  end

  describe 'PATCH #update' do
    it('updates the profile') do
      profile = create(:profile, last_name: 'Before')
      new_last_name = 'A new name'

      patch admin_profile_path(as: @user, id: profile.id), params: {
        profile: { last_name: new_last_name }
      }

      expect(flash[:success]).to match 'Profile updated'
      expect(response).to redirect_to(admin_profile_path(profile))
      profile.reload
      expect(profile.last_name).to eq new_last_name
    end
  end

  describe 'DELETE destroy' do
    it('deletes the profile') do
      profile = create(:profile)

      expect do
        delete admin_profile_path(as: @user, id: profile.id)
      end.to(change { Profile.count }.by(-1))
      expect(response).to redirect_to(admin_profiles_path)
    end
  end

  describe 'GET show' do
    it('gets profile') do
      profile = create(:profile)

      get admin_profile_path(as: @user, id: profile.id)

      expect(response).to be_successful
      expect(response.body).to include profile.website_with_protocol
    end
  end

  describe 'GET index' do
    it('should get index when user is signed in') do
      get admin_profiles_path(as: @user)

      expect(response).to be_successful
      expect(response.body).to include 'Administration'
    end

    it('filters by published state') do
      published_profile = create(:published_profile)
      unpublished_profile = create(:unpublished_profile)

      get admin_profiles_path(as: @user), params: { published: 'true' }

      expect(response.body).to include published_profile.last_name
      expect(response.body).not_to include unpublished_profile.last_name
    end

    it('should return a routing error when no user is signed in') do
      expect do
        get admin_profiles_path
      end.to(raise_error(ActionController::RoutingError))
    end
  end
end
