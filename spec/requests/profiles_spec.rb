# frozen_string_literal: true

require('rails_helper')

describe 'Profiles', type: :request do
  before do
    @profile = create(:published_profile)
    @address = create(:address, state: 'DE-BW', profile_id: @profile.id)
    @tag = create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    @treatment_method = create(:treatment_method, name: {
                                 en: 'Abortion', de: 'Schwangerschaftsabbruch'
                               })
    create(:profile_treatment, treatment_method_id: @treatment_method.id,
                               profile_id: @profile.id)
    create(:tagging, tag_id: @tag.id, profile_id: @profile.id)
  end

  describe 'GET show' do
    it('returns a success response') do
      get profile_path(@profile.id)

      expect(response).to be_successful
      expect(response.body).to include(@profile.website_with_protocol)
    end
  end

  describe 'GET index' do
    it('returns a success response') do
      get profiles_path

      expect(response).to be_successful
      expect(response.body).to include(@profile.last_name)
    end

    it('does not list unpublished profiles') do
      unpublished_profile = create(:unpublished_profile)

      get profiles_path

      expect(response.body).not_to include(unpublished_profile.last_name)
    end

    it('filters by tag') do
      second_profile = create(
        :profile,
        last_name: 'Second Profile'
      )
      second_tag = create(:tag, name: {
                            en: 'Queer friendly',
                            de: 'Queer freundlich'
                          })
      create(:tagging, tag_id: second_tag.id, profile_id: second_profile.id)

      get profiles_path, params: { tag: ['trans_friendly'] }

      expect(response.body).to include(@profile.last_name)
      expect(response.body).not_to include(second_profile.last_name)
    end

    it('index filters by tag, state and treatment_method') do
      second_profile = create(:profile, last_name: 'Second Profile')
      second_tag = create(:tag, name: {
                            en: 'Queer friendly',
                            de: 'Queer freundlich'
                          })
      create(:tagging, tag_id: second_tag.id, profile_id: second_profile.id)
      create(:address, state: 'DE-HH', profile_id: second_profile.id)
      treatment_method = create(:treatment_method,
                                name: { en: 'Sterilization',
                                        de: 'Sterilisation' },
                                key: 'sterilization')
      create(:profile_treatment, treatment_method_id: treatment_method.id,
                                 profile_id: second_profile.id)

      get profiles_path, params: {
        tag: ['trans_friendly'], state: 'DE-BW', treatment_method: ['abortion']
      }

      expect(response.body).to include(@profile.last_name)
      expect(response.body).not_to include(second_profile.last_name)
    end
  end
end
