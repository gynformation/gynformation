# frozen_string_literal: true

require('rails_helper')

RSpec.describe(Questionnaire, type: :model) do
  context 'associations' do
    it { is_expected.to belong_to(:profile).without_validating_presence }
    it { is_expected.to have_many(:answers) }

    it { is_expected.to have_many(:questionnaire_taggings) }
    it { is_expected.to have_many(:tags) }

    it { is_expected.to have_many(:questionnaire_treatments) }
    it { is_expected.to have_many(:treatment_methods) }
  end

  it('sorts by recently created') do
    first_questionnaire = create(:questionnaire)
    second_questionnaire = create(:questionnaire)

    expect(Questionnaire.first).to(eq(second_questionnaire))
    expect(Questionnaire.last).to(eq(first_questionnaire))
  end

  describe '#treating_persons_name' do
    it('returns the full name of the treating persons') do
      answer1 = create(:answer, key: 'first_name', content: 'Hanni')
      answer2 = create(:answer, key: 'last_name', content: 'Example')
      questionnaire = create(:questionnaire, answers: [answer1, answer2])

      expect(questionnaire.treating_persons_name).to(eq('Hanni Example'))
    end

    it('returns nil when names are not present') do
      questionnaire = create(:questionnaire)

      expect(questionnaire.treating_persons_name).to(be_nil)
    end
  end

  it('is not valid with invalid status') do
    expect do
      create(:questionnaire, status: :invalid)
    end.to(raise_error(ArgumentError, "'invalid' is not a valid status"))
  end

  it('sets "new" as default value for status') do
    questionnaire = Questionnaire.new
    expect(questionnaire.status).to(eq('new'))
  end

  describe('#filter_by_status') do
    it('filters by status') do
      questionnaire1 = create(:questionnaire, status: :new)
      questionnaire2 = create(:questionnaire, status: :profile_published)

      result = Questionnaire.filter_by_status('new')

      expect(result).to include(questionnaire1)
      expect(result).not_to include(questionnaire2)
    end
  end

  describe '#filter_by_last_name' do
    it 'filters by last name' do
      answer1 = create(:answer, key: 'last_name', content: 'Smith')
      answer2 = create(:answer, key: 'last_name', content: 'Meyer')
      questionnaire1 = create(:questionnaire, answers: [answer1])
      questionnaire2 = create(:questionnaire, answers: [answer2])

      result = Questionnaire.filter_by_last_name('smith')

      expect(result).to include(questionnaire1)
      expect(result).not_to include(questionnaire2)
    end
  end
end
