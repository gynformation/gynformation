# frozen_string_literal: true

require('rails_helper')

RSpec.describe(Address, type: :model) do
  it('is valid with valid attributes') do
    profile = create(:profile)
    address = Address.new(street: 'Bakerstreet 121', zip: '20148',
                          city: 'Hamburg', state: 'DE-HH',
                          profile_id: profile.id)

    expect(address.valid?).to(eq(true))
  end

  it('is not valid with an invalid state') do
    profile = create(:profile)
    address = Address.new(state: 'INVALID', profile_id: profile.id)
    expect(address.invalid?).to(eq(true))
    expect(['INVALID is not a valid state']).to(eq(address.errors[:state]))
  end
end
