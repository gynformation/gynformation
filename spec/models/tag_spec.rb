# frozen_string_literal: true

require('rails_helper')

RSpec.describe(Tag, type: :model) do
  it 'returns a name for a given locale' do
    tag = Tag.create(name: { en: 'Trans friendly', de: 'Trans freundlich' })
    german_tag_name = tag.name['de']

    expect(german_tag_name).to eq 'Trans freundlich'
  end

  describe '#tag_list' do
    it('returns all tags of the profile') do
      profile = create(:profile)
      tag1 = create(:tag, key: 'trans_friendly', name:
        { en: 'Trans friendly', de: 'Trans freundlich' })
      tag2 = create(:tag, key: 'sexworker_friendly', name:
        { en: 'Sex worker friendly', de: 'Sexworkerinnen freundlich' })
      create(:tagging, tag_id: tag1.id, profile_id: profile.id)
      create(:tagging, tag_id: tag2.id, profile_id: profile.id)

      tag_list = profile.tag_list

      expected_tag_list = '{"de"=>"Trans freundlich", ' \
                          '"en"=>"Trans friendly"}, {"de"=>"Sexworkerinnen freundlich", ' \
                          '"en"=>"Sex worker friendly"}'

      expect(tag_list).to(eq(expected_tag_list))
    end
  end
end
