# frozen_string_literal: true

require('rails_helper')

RSpec.describe(QuestionnaireTagging, type: :model) do
  context 'associations' do
    it { is_expected.to belong_to(:tag) }
    it { is_expected.to belong_to(:questionnaire) }
  end
end
