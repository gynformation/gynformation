# frozen_string_literal: true

require('rails_helper')

RSpec.describe(TreatmentMethod, type: :model) do
  it('returns the name for a given locale') do
    treatment_method = TreatmentMethod.create(name:
      { en: 'English treatment method', de: 'German treatment method' })
    german_treatment_method_name = treatment_method.name['de']

    expect(german_treatment_method_name).to eq 'German treatment method'
  end
end
