# frozen_string_literal: true

require('rails_helper')

RSpec.describe(Post, type: :model) do
  context 'associations' do
    it { is_expected.to have_many(:post_taggings) }
    it { is_expected.to have_many(:post_tags) }
  end

  context 'default scope' do
    it 'sorts by published date' do
      post1 = create(:post, publish_date: Date.yesterday)
      post2 = create(:post, publish_date: Date.tomorrow)

      expect(Post.all).to eq [post2, post1]
    end
  end

  describe 'attachements' do
    it 'attaches the main image' do
      post = create(:post)

      post.main_image.attach(
        io: Rails.root.join('app/assets/images/condoms.png').open,
        filename: 'condom.png', content_type: 'image/png'
      )

      expect(post.main_image).to be_attached
    end

    it 'attaches other images' do
      post = create(:post)

      post.other_images.attach(
        io: Rails.root.join('app/assets/images/condoms.png').open,
        filename: 'condom.png', content_type: 'image/png'
      )

      expect(post.other_images).to be_attached
    end
  end
end
