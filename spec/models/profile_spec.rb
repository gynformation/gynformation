# frozen_string_literal: true

require('rails_helper')

RSpec.describe(Profile, type: :model) do
  context 'associations' do
    it { is_expected.to have_one(:address) }
    it { is_expected.to have_many(:taggings) }
    it { is_expected.to have_many(:tags) }
    it { is_expected.to have_many(:profile_treatments) }
    it { is_expected.to have_many(:questionnaires) }
    it { is_expected.to have_many(:comments) }
  end

  it('is valid with valid attributes') do
    profile = Profile.new(first_name: 'Carla', last_name: 'Smith',
                          category: :gynecologist, website_with_protocol: 'https://www.example.org')
    expect(profile).to be_valid
  end

  it('is not valid without name') do
    profile = Profile.new(category: :gynecologist, website_with_protocol: 'https://www.example.org')
    expect(profile).not_to be_valid
  end

  it('is not valid without category') do
    profile = Profile.new(category: nil, last_name: 'Smith')
    expect(profile).not_to be_valid
  end

  it('is not valid with an invalid category') do
    expect do
      Profile.new(first_name: 'Carla', last_name: 'Smith', category: :invalid)
    end.to raise_error(ArgumentError, "'invalid' is not a valid category")
  end

  it('is valid when website has http or https protocol') do
    http_profile = Profile.new(
      first_name: 'Carla',
      last_name: 'Smith',
      category: :gynecologist,
      website_with_protocol: 'http://www.example.org'
    )
    expect(http_profile).to be_valid

    https_profile = Profile.new(
      first_name: 'Carla',
      last_name: 'Smith',
      category: :gynecologist,
      website_with_protocol: 'https://www.example.org'
    )
    expect(https_profile).to be_valid
  end

  it('is not valid when website has no protocol') do
    profile = Profile.new(
      first_name: 'Carla',
      last_name: 'Smith',
      category: :gynecologist,
      website_with_protocol: 'www.example.org'
    )
    expect(profile).not_to be_valid
  end

  it('sets published to false by default') do
    profile = create(:profile)
    expect(false).to(eq(profile.published))
  end

  describe('#filter_by_tag') do
    it('filters by tag') do
      profile = create(:profile)
      one_tag_profile = create(:profile)
      tag1 = create(:tag, key: 'trans_friendly')
      tag2 = create(:tag, key: 'queer_friendly')
      create(:tagging, tag_id: tag1.id, profile_id: profile.id)
      create(:tagging, tag_id: tag2.id, profile_id: profile.id)

      create(:tagging, tag_id: tag2.id, profile_id: one_tag_profile.id)

      result = Profile.filter_by_tag(['queer_friendly'])

      expect(result).to include(profile)
      expect(result).to include(one_tag_profile)
    end

    it('only includes profiles with all selected tags') do
      profile = create(:profile)
      one_tag_profile = create(:profile)
      tag1 = create(:tag, key: 'trans_friendly')
      tag2 = create(:tag, key: 'queer_friendly')
      create(:tagging, tag_id: tag1.id, profile_id: profile.id)
      create(:tagging, tag_id: tag2.id, profile_id: profile.id)

      create(:tagging, tag_id: tag2.id, profile_id: one_tag_profile.id)

      result = Profile.filter_by_tag(%w[queer_friendly trans_friendly])

      expect(result).to include(profile)
      expect(result).not_to include(one_tag_profile)
    end

    it('does not return anything if no profile has all selected tags') do
      profile = create(:profile)
      other_profile = create(:profile)
      tag1 = create(:tag, key: 'trans_friendly')
      tag2 = create(:tag, key: 'queer_friendly')
      create(:tagging, tag_id: tag1.id, profile_id: profile.id)
      create(:tagging, tag_id: tag2.id, profile_id: other_profile.id)

      result = Profile.filter_by_tag(%w[queer_friendly trans_friendly])

      expect(result.length).to(eq(0))
    end
  end

  describe('#filter_by_treatment_method') do
    it('filters by treatment methods') do
      profile = create(:profile)
      other_profile = create(:profile)
      treatment_method1 = create(:treatment_method, key: 'abortion')
      treatment_method2 = create(:treatment_method, key: 'examination')
      create(:profile_treatment, treatment_method_id: treatment_method1.id,
                                 profile_id: profile.id)
      create(:profile_treatment, treatment_method_id: treatment_method2.id,
                                 profile_id: profile.id)

      create(:profile_treatment, treatment_method_id: treatment_method1.id,
                                 profile_id: other_profile.id)

      result = Profile.filter_by_treatment_method(['abortion'])
      expect(result).to include(profile)
      expect(result).to include(other_profile)
    end

    it('only includes profiles with all selected treatment methods') do
      profile = create(:profile)
      other_profile = create(:profile)
      treatment_method1 = create(:treatment_method, key: 'abortion')
      treatment_method2 = create(:treatment_method, key: 'examination')
      create(:profile_treatment, treatment_method_id: treatment_method1.id,
                                 profile_id: profile.id)
      create(:profile_treatment, treatment_method_id: treatment_method2.id,
                                 profile_id: profile.id)

      create(:profile_treatment, treatment_method_id: treatment_method1.id,
                                 profile_id: other_profile.id)

      result = Profile.filter_by_treatment_method(%w[abortion examination])
      expect(result).to include(profile)
      expect(result).not_to include(other_profile)
    end

    it('does not return anything if no profile has all selected treatment methods') do
      profile = create(:profile)
      other_profile = create(:profile)
      treatment_method1 = create(:treatment_method, key: 'abortion')
      treatment_method2 = create(:treatment_method, key: 'examination')
      create(:profile_treatment, treatment_method_id: treatment_method1.id,
                                 profile_id: profile.id)

      create(:profile_treatment, treatment_method_id: treatment_method2.id,
                                 profile_id: other_profile.id)

      result = Profile.filter_by_treatment_method(['examination, abortion'])
      expect(result.length).to(eq(0))
    end
  end

  describe('#filter_by_state') do
    it('filters by state') do
      profile = create(:profile)
      create(:address, profile_id: profile.id, state: 'DE-HH')
      profile_with_different_state = create(:profile)

      result = Profile.filter_by_state('DE-HH')

      expect(result).to include(profile)
      expect(result).not_to include(profile_with_different_state)
    end
  end

  describe('#filter_by_published') do
    it('filters by published') do
      published_profile = create(:published_profile)
      unpublished_profile = create(:unpublished_profile)

      result = Profile.filter_by_published('true')

      expect(result).to include(published_profile)
      expect(result).not_to include(unpublished_profile)
    end
  end

  describe('#filter_by_last_name') do
    it('filters by last name') do
      profile1 = create(:profile, last_name: 'Meier')
      profile2 = create(:profile, last_name: 'Schmidt')

      result = Profile.filter_by_last_name('Meier')

      expect(result).to include(profile1)
      expect(result).not_to include(profile2)
    end

    it('filters case insensitive partial matches') do
      profile1 = create(:profile, last_name: 'Meier')
      profile2 = create(:profile, last_name: 'Schmidt')

      result = Profile.filter_by_last_name('meie')

      expect(result).to include(profile1)
      expect(result).not_to include(profile2)
    end
  end

  describe('#filter_by_category') do
    it('filters by category') do
      profile1 = create(:profile, category: 'gynecologist')
      profile2 = create(:profile, category: 'midwife')

      result = Profile.filter_by_category('gynecologist')

      expect(result).to include(profile1)
      expect(result).not_to include(profile2)
    end
  end
end
