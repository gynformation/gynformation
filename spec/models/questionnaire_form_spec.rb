# frozen_string_literal: true

require('rails_helper')

RSpec.describe(QuestionnaireForm, type: :model) do
  describe '#save' do
    it('creates the questionnaire and the answers ') do
      params = { 'first_name' => 'Lora', 'last_name' => 'Ipsum',
                 'category' => 'general_practitioner',
                 'payments_required_for_specific_treatments' =>
        { 'boolean' => 'true', 'website_with_protocol' => 'https://www.example.org' },
                 'tag_ids' => [],
                 'treatment_method_ids' => [] }
      questionnaire_form = QuestionnaireForm.new(params)

      expect do
        expect { questionnaire_form.save }.to(change { Answer.count }.by(31))
      end.to(change { Questionnaire.count }.by(1))
    end

    it('associates treatment methods when treatmend_ids are given') do
      treatment_method1 = create(:treatment_method, key: 'abortion')
      treatment_method2 = create(:treatment_method, key: 'examination')

      params = { 'treatment_method_ids' => [treatment_method1.id, treatment_method2.id] }
      questionnaire_form = QuestionnaireForm.new(params)
      questionnaire_form.save

      expect(QuestionnaireTreatment.count).to(eq(2))
    end

    it('associates tags when tag_ids are given') do
      tag1 = create(:tag, key: 'trans_friendly')
      tag2 = create(:tag, key: 'queer_friendly')

      params = { 'tag_ids' => [tag1.id, tag2.id] }
      questionnaire_form = QuestionnaireForm.new(params)
      questionnaire_form.save

      expect(QuestionnaireTagging.count).to(eq(2))
    end

    it('saves successfully when no treatment_ids are given') do
      params = { 'first_name' => 'Lora', 'last_name' => 'Ipsum',
                 'treatment_method_ids' => [] }
      questionnaire_form = QuestionnaireForm.new(params)

      expect do
        expect { questionnaire_form.save }.to(change { Answer.count }.by(31))
      end.to(change { Questionnaire.count }.by(1))

      expect(QuestionnaireTreatment.count).to(eq(0))
    end

    it('saves successfully when no tag_ids are given') do
      params = { 'first_name' => 'Lora', 'last_name' => 'Ipsum',
                 'tag_ids' => [] }
      questionnaire_form = QuestionnaireForm.new(params)

      expect do
        expect { questionnaire_form.save }.to(change { Answer.count }.by(31))
      end.to(change { Questionnaire.count }.by(1))

      expect(QuestionnaireTagging.count).to(eq(0))
    end
  end
end
