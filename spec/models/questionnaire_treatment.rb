# frozen_string_literal: true

require('rails_helper')

RSpec.describe(QuestionnaireTreatment, type: :model) do
  context 'associations' do
    it { is_expected.to belong_to(:treatment_method) }
    it { is_expected.to belong_to(:questionnaire) }
  end
end
