# frozen_string_literal: true

require('rails_helper')

RSpec.describe(Answer, type: :model) do
  it('sorts ascending by created at') do
    first_answer = create(:answer)
    second_answer = create(:answer)
    expect(Answer.first).to(eq(first_answer))
    expect(Answer.last).to(eq(second_answer))
  end

  describe '#answer' do
    it('returns the content of the answer with the given key') do
      answer = create(:answer, key: 'a-key', content: 'This is the answer')
      questionnaire = create(:questionnaire, answers: [answer])
      expect('This is the answer').to(eq(questionnaire.answer('a-key')))
    end

    it('returns an empty hash {} for a non existing key') do
      questionnaire = create(:questionnaire)
      expect('').to(eq(questionnaire.answer('non-existing-key')))
    end
  end
end
