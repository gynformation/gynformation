# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProfileDecorator do
  it('is decortated correctly with complete address') do
    profile = create(:published_profile)
    create(:address, profile_id: profile.id)

    display_full_address = 'Bakerstreet 121, 20148 Hamburg,'
    expect(ProfileDecorator.new(profile).display_address)
      .to eq display_full_address
  end

  it('is decortated correctly without street') do
    profile = create(:published_profile)
    create(:address_without_street, profile_id: profile.id)

    display_no_street = '20148 Hamburg,'
    expect(ProfileDecorator.new(profile).display_address)
      .to eq display_no_street
  end

  it('is decortated correctly without street and zip') do
    profile = create(:published_profile)
    create(:address_without_zip, profile_id: profile.id)

    display_no_zip = 'Hamburg,'
    expect(ProfileDecorator.new(profile).display_address)
      .to eq display_no_zip
  end

  it('is decortated correctly without street, zip and city') do
    profile = create(:published_profile)
    create(:address_without_city, profile_id: profile.id)

    display_no_address = ''
    expect(ProfileDecorator.new(profile).display_address)
      .to eq display_no_address
  end
end
