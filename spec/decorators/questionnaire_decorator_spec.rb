# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestionnaireDecorator do
  it('is decortated correctly with complete address') do
    answer1 = create(:answer, key: 'street', content: 'Bakerstreet 121')
    answer2 = create(:answer, key: 'zip_code', content: '20148')
    answer3 = create(:answer, key: 'city', content: 'Hamburg')
    questionnaire = create(:questionnaire, answers: [answer1, answer2, answer3])

    display_full_address = 'Bakerstreet 121, 20148 Hamburg,'
    expect(QuestionnaireDecorator.new(questionnaire).display_address)
      .to eq display_full_address
  end

  it('is decortated correctly without street') do
    answer1 = create(:answer, key: 'zip_code', content: '20148')
    answer2 = create(:answer, key: 'city', content: 'Hamburg')
    questionnaire = create(:questionnaire, answers: [answer1, answer2])

    display_no_street = '20148 Hamburg,'
    expect(QuestionnaireDecorator.new(questionnaire).display_address)
      .to eq display_no_street
  end

  it('is decortated correctly without street and zip') do
    answer1 = create(:answer, key: 'city', content: 'Hamburg')
    questionnaire = create(:questionnaire, answers: [answer1])

    display_no_zip = 'Hamburg,'
    expect(QuestionnaireDecorator.new(questionnaire).display_address)
      .to eq display_no_zip
  end

  it('is decortated correctly without street, zip and city') do
    questionnaire = create(:questionnaire)

    display_no_address = ''
    expect(QuestionnaireDecorator.new(questionnaire).display_address)
      .to eq display_no_address
  end

  it('is decortated correctly when no answer was given') do
    questionnaire = create(:questionnaire)

    expect(QuestionnaireDecorator.new(questionnaire).display_answer('website'))
      .to eq '-'
  end

  it('is decortated correctly when  answer was given') do
    answer1 = create(:answer, key: 'website', content: 'www.test.de')
    questionnaire = create(:questionnaire, answers: [answer1])

    expect(QuestionnaireDecorator.new(questionnaire).display_answer('website'))
      .to eq 'www.test.de'
  end
end
