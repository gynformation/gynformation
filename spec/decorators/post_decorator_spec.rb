# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostDecorator do
  it('is decortated correctly with image title and credit') do
    post = create(:post)

    display_title_and_credit = 'This is the title of the image. / Test credit'
    expect(PostDecorator.new(post).display_title_credit)
      .to eq display_title_and_credit
  end

  it('is decortated correctly with image title without credit') do
    post = create(:post_no_main_image_credit)

    display_title_no_credit = 'This is the title of the image.'
    expect(PostDecorator.new(post).display_title_credit)
      .to eq display_title_no_credit
  end

  it('is decortated correctly with image credit without title') do
    post = create(:post_no_main_image_title)

    display_credit_no_title = 'Test credit'
    expect(PostDecorator.new(post).display_title_credit)
      .to eq display_credit_no_title
  end
end
