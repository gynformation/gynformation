# Grab an issue

If you want to contribute, you can get an overview over the open issues on our [Project Management Board](https://gitlab.com/sssggr/gynformation/-/boards/1318691). If you want to work on an issue, assign it to yourself. If you have an idea for a new feature or functionality, open up an issue. Let us know, if you have any questions. 

# Workflow

1. Let us know, on what you are working through an issue on our board.
2. Clone or fork the repository and work in a meaningful named branch that is based off of our "main" branch.
3. Make your changes
4. Make sure, you add tests and keep our linter (rubocop) happy
5. Create a merge request
