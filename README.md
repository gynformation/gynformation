# README

## About Gynformation

Gynformation is a a curated web directory to find gynecological treatment. We are a group of queer-feminist activists who advocate self-determination in the context of gynaecological treatment from a patient perspective.  

## Development setup

* Install [ruby](https://www.ruby-lang.org/en/documentation/installation/)  
* Install [postgres](https://www.postgresql.org/) and make sure it is running locally.
* Install [bundler](https://bundler.io/)
* Install [yarn](https://yarnpkg.com/)
* Clone this repository
* Run `bundle install`  inside of the project directory to install all dependencies and `rake db:setup` to initialize the database.
* Run `yarn install` to pull javascript dependencies
* For Windows: "workers Integer(ENV['WEB_CONCURRENCY'] || 2)" might have to be removed from puma.rb
* Start the development server `bin/rails server` and visit

### Seeds
* Run `rake db:seed` to populate the database 
* For the CMS use `rake 'comfy:cms_seeds:import[gynformation, gynformation]'`

## Testing

Use `bundle exec rspec` to run the tests

## Linting

We use [rubocop](https://github.com/rubocop-hq/rubocop) to lint the code. 

`gem install rubocop`

The checks run in the pipeline, to check locally run:

`rubocop -a`

## Deployment

The main branch ist continously deployed to our staging environment.
Production deployment needs manual approval in the gitlab pipeline. 

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

## Contributing

Do you want to contribute? Great! 

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for further details.

## License

GNU GPL 3.0. Please see LICENSE.txt.
