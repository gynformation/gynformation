# frozen_string_literal: true

Rails.application.routes.draw do
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    resources :passwords, controller: 'clearance/passwords', only: %i[create new]
    resource :session, controller: 'clearance/sessions', only: [:create]

    resources :users, controller: 'clearance/users', only: [:create] do
      resource :password,
              controller: 'clearance/passwords',
              only: %i[create edit update]
    end

    get '/sign_in' => 'clearance/sessions#new', as: 'sign_in'
    delete '/sign_out' => 'clearance/sessions#destroy', as: 'sign_out'
    
    match 'blobs/:signed_id/*filename', to: 'blobs#show', via: [:get, :post]

    root 'static_pages#home'
    get 'static_pages/home', to: 'static_pages#home'

    resources :profiles, only: %i[index show]
    resources :questionnaire, only: %i[new create]
    get 'questionnaire/info', to: 'questionnaire#info'
    resources :posts, only: %i[index show]

    constraints Clearance::Constraints::SignedIn.new do
      comfy_route :cms_admin, path: "/admin/cms"
      comfy_route :blog_admin, path: "/admin/cms"
      get 'admin/profiles/search', to: 'admin/profiles#search',  defaults: { format: 'js' }

      namespace :admin do
        resources :profiles
        resources :dashboard, only: %i[index]
        resources :questionnaires, only: %i[index edit update destroy]
        resources :tags
        resources :post_tags
        resources :treatment_methods
        resources :posts
      end

      mount Tolk::Engine, at: 'admin/translations'
      mount Blazer::Engine, at: "/admin/statistics"
    end
    # Has to be defined last or it will make all other routes to be inaccessible.
    # comfy_route :blog, path: "/blog"
    comfy_route :cms, path: "/"
  end
end
