# encoding: utf-8

# Tolk config file. Generated on October 04, 2020 16:44
# See github.com/tolk/tolk for more informations

Tolk.config do |config|
  config.exclude_gems_token = true
  config.ignore_keys = ['faker', 'clearance', 'clearance_mailer']
end
